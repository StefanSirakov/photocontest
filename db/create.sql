-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия на сървъра:            10.6.5-MariaDB - mariadb.org binary distribution
-- ОС на сървъра:                Win64
-- HeidiSQL Версия:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Дъмп на структурата на БД photo_contest
CREATE DATABASE IF NOT EXISTS `photo_contest` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `photo_contest`;

-- Дъмп структура за таблица photo_contest.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_id_uindex` (`id`),
  UNIQUE KEY `categories_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица photo_contest.contests
CREATE TABLE IF NOT EXISTS `contests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_open` tinyint(1) NOT NULL DEFAULT 1,
  `phase` int(11) NOT NULL,
  `contest_starting_time` datetime NOT NULL DEFAULT current_timestamp(),
  `phase_two_starting_time` datetime NOT NULL,
  `contest_ending_time` datetime NOT NULL,
  `background_image` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contests_id_uindex` (`id`),
  UNIQUE KEY `contests_title_uindex` (`title`),
  KEY `contests_join_types_id_fk` (`is_open`),
  KEY `contests_categories_id_fk` (`category_id`),
  KEY `contests_users_id_fk` (`user_id`),
  CONSTRAINT `contests_categories_id_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `contests_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица photo_contest.contests_categories
CREATE TABLE IF NOT EXISTS `contests_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contest_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contests_categories_id_uindex` (`id`),
  KEY `contests_categories_categories_id_fk` (`category_id`),
  KEY `contests_categories_contests_id_fk` (`contest_id`),
  CONSTRAINT `contests_categories_categories_id_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `contests_categories_contests_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица photo_contest.contests_jurors
CREATE TABLE IF NOT EXISTS `contests_jurors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contest_id` int(11) NOT NULL,
  `juror_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contest_jurors_id_uindex` (`id`),
  KEY `contest_jurors_contests_id_fk` (`contest_id`),
  KEY `contest_jurors_users_id_fk` (`juror_id`),
  CONSTRAINT `contest_jurors_contests_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`),
  CONSTRAINT `contest_jurors_users_id_fk` FOREIGN KEY (`juror_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица photo_contest.contest_invited_junkies
CREATE TABLE IF NOT EXISTS `contest_invited_junkies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contest_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contest_invited_junkies_id_uindex` (`id`),
  KEY `contest_invited_junkies_contests_id_fk` (`contest_id`),
  KEY `contest_invited_junkies_users_id_fk` (`user_id`),
  CONSTRAINT `contest_invited_junkies_contests_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`),
  CONSTRAINT `contest_invited_junkies_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица photo_contest.contest_participants
CREATE TABLE IF NOT EXISTS `contest_participants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contest_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contest_participants_id_uindex` (`id`),
  KEY `contest_participants_contests_id_fk` (`contest_id`),
  KEY `contest_participants_users_id_fk` (`user_id`),
  CONSTRAINT `contest_participants_contests_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`),
  CONSTRAINT `contest_participants_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица photo_contest.contest_winners
CREATE TABLE IF NOT EXISTS `contest_winners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contest_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `place` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contest_winners_id_uindex` (`id`),
  KEY `contest_winners_contests_id_fk` (`contest_id`),
  KEY `contest_winners_images_id_fk` (`image_id`),
  CONSTRAINT `contest_winners_contests_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`),
  CONSTRAINT `contest_winners_images_id_fk` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица photo_contest.images
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `story` text NOT NULL,
  `contest_id` int(11) NOT NULL,
  `total_score` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `photos_id_uindex` (`id`),
  UNIQUE KEY `images_title_uindex` (`title`) USING HASH,
  KEY `images_contests_id_fk` (`contest_id`),
  KEY `images_users_id_fk` (`user_id`),
  CONSTRAINT `images_contests_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`),
  CONSTRAINT `images_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица photo_contest.images_reviews
CREATE TABLE IF NOT EXISTS `images_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `review_id` int(11) NOT NULL,
  UNIQUE KEY `images_reviews_id_uindex` (`id`),
  UNIQUE KEY `images_reviews_id_uindex_2` (`id`),
  KEY `images_reviews_images_id_fk` (`image_id`),
  KEY `images_reviews_review_id_fk` (`review_id`),
  CONSTRAINT `images_reviews_images_id_fk` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`),
  CONSTRAINT `images_reviews_review_id_fk` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица photo_contest.ranks
CREATE TABLE IF NOT EXISTS `ranks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ranks_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица photo_contest.review
CREATE TABLE IF NOT EXISTS `review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `score` int(11) NOT NULL,
  `comment` varchar(500) NOT NULL,
  `juror_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `review_id_uindex` (`id`),
  KEY `review_users_id_fk` (`juror_id`),
  CONSTRAINT `review_users_id_fk` FOREIGN KEY (`juror_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица photo_contest.scores
CREATE TABLE IF NOT EXISTS `scores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `scores_id_uindex` (`id`),
  KEY `scores_photos_id_fk` (`photo_id`),
  KEY `scores_users_id_fk` (`user_id`),
  CONSTRAINT `scores_photos_id_fk` FOREIGN KEY (`photo_id`) REFERENCES `images` (`id`),
  CONSTRAINT `scores_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

-- Дъмп структура за таблица photo_contest.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `password` varchar(30) NOT NULL,
  `is_organiser` tinyint(1) NOT NULL DEFAULT 0,
  `rank_id` int(11) NOT NULL,
  `points` int(11) NOT NULL DEFAULT 0,
  `email` varchar(50) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `reset_password_token` varchar(30) DEFAULT NULL,
  `avatar` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_id_uindex` (`id`),
  UNIQUE KEY `users_username_uindex` (`username`),
  UNIQUE KEY `users_email_uindex` (`email`),
  KEY `users_ranks_id_fk` (`rank_id`),
  CONSTRAINT `users_ranks_id_fk` FOREIGN KEY (`rank_id`) REFERENCES `ranks` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Изнасянето на данните беше деселектирано.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
