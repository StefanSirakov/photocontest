-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия на сървъра:            10.6.5-MariaDB - mariadb.org binary distribution
-- ОС на сървъра:                Win64
-- HeidiSQL Версия:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Дъмп данни за таблица photo_contest.categories: ~12 rows (приблизително)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`) VALUES
	(21, 'autumn'),
	(23, 'buildings'),
	(12, 'cats'),
	(13, 'dogs'),
	(18, 'Mountains'),
	(24, 'people'),
	(19, 'pets'),
	(22, 'sports'),
	(14, 'Spring'),
	(20, 'summer'),
	(16, 'sunset'),
	(15, 'wild animals');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Дъмп данни за таблица photo_contest.contests: ~13 rows (приблизително)
/*!40000 ALTER TABLE `contests` DISABLE KEYS */;
INSERT INTO `contests` (`id`, `title`, `category_id`, `user_id`, `is_open`, `phase`, `contest_starting_time`, `phase_two_starting_time`, `contest_ending_time`, `background_image`) VALUES
	(49, 'Best cat photos', 12, 16, 1, 3, '2022-04-27 11:01:51', '2022-04-27 11:30:00', '2022-04-27 11:40:00', 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\contestbackgrounds\\wallhaven-0joz5n.jpg'),
	(50, 'Best dogs photos', 13, 17, 1, 3, '2022-04-27 13:06:32', '2022-04-27 13:12:00', '2022-04-27 13:20:00', 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\contestbackgrounds\\01.11.2021_SGD1014-Edit-scaled.jpg'),
	(51, 'Spring captures', 14, 16, 1, 3, '2022-04-27 13:24:06', '2022-04-27 13:30:00', '2022-04-27 13:35:00', 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\contestbackgrounds\\91d92318f0930eebe21b013ff89d5cbd.jpg'),
	(53, 'Wild Animals', 15, 16, 1, 3, '2022-04-27 23:15:42', '2022-04-27 23:20:00', '2022-04-27 23:22:00', 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\contestbackgrounds\\photo-1575550959106-5a7defe28b56.jpg'),
	(54, 'Best Sunset Shots', 16, 17, 1, 3, '2022-04-27 23:23:45', '2022-04-27 23:30:00', '2022-04-27 23:33:00', 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\contestbackgrounds\\1000.jpg'),
	(56, 'Mountains', 18, 16, 1, 3, '2022-04-27 23:38:00', '2022-04-27 23:42:00', '2022-04-27 23:45:00', 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\contestbackgrounds\\photo-1519681393784-d120267933ba.jpg'),
	(57, 'Cats and cats again', 12, 16, 1, 3, '2022-04-27 23:48:21', '2022-04-27 23:53:00', '2022-04-27 23:57:00', 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\contestbackgrounds\\LIbvrV.jpg'),
	(59, 'Pets time!', 19, 17, 0, 3, '2022-04-28 00:00:51', '2022-04-28 00:05:00', '2022-04-28 00:08:00', 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\contestbackgrounds\\dog-and-cat.jpg'),
	(60, 'Summer Time!!!', 20, 16, 1, 3, '2022-04-28 00:11:12', '2022-04-28 00:15:00', '2022-04-28 00:20:00', 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\contestbackgrounds\\253946724.jpg'),
	(61, 'Autumn shots', 21, 16, 1, 3, '2022-04-28 00:27:33', '2022-04-28 00:40:00', '2022-04-28 00:48:00', 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\contestbackgrounds\\fall-autumn-red-season.jpg'),
	(62, 'Sports', 22, 16, 1, 1, '2022-04-28 12:22:59', '2022-04-30 12:22:00', '2022-05-01 12:22:00', 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\contestbackgrounds\\wallpaperflare.com_wallpaper.jpg'),
	(63, 'Futuristic Homes', 23, 16, 1, 2, '2022-04-28 13:02:11', '2022-04-28 13:10:00', '2022-04-30 13:02:00', 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\contestbackgrounds\\john-fornander-Id7u0EkTjBE-unsplash.jpg'),
	(64, 'Portraits', 24, 16, 0, 3, '2022-04-28 13:27:56', '2022-04-28 13:32:00', '2022-04-28 13:36:00', 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\contestbackgrounds\\canon-portrait-photography-techniques_1934-1_c.jpg');
/*!40000 ALTER TABLE `contests` ENABLE KEYS */;

-- Дъмп данни за таблица photo_contest.contests_categories: ~13 rows (приблизително)
/*!40000 ALTER TABLE `contests_categories` DISABLE KEYS */;
INSERT INTO `contests_categories` (`id`, `contest_id`, `category_id`) VALUES
	(19, 49, 12),
	(20, 50, 13),
	(21, 51, 14),
	(23, 53, 15),
	(24, 54, 16),
	(26, 56, 18),
	(27, 57, 12),
	(29, 59, 19),
	(30, 60, 20),
	(31, 61, 21),
	(32, 62, 22),
	(33, 63, 23),
	(34, 64, 24);
/*!40000 ALTER TABLE `contests_categories` ENABLE KEYS */;

-- Дъмп данни за таблица photo_contest.contests_jurors: ~31 rows (приблизително)
/*!40000 ALTER TABLE `contests_jurors` DISABLE KEYS */;
INSERT INTO `contests_jurors` (`id`, `contest_id`, `juror_id`) VALUES
	(55, 49, 16),
	(56, 49, 17),
	(57, 50, 16),
	(58, 50, 17),
	(59, 51, 16),
	(60, 51, 17),
	(64, 53, 16),
	(65, 53, 20),
	(66, 53, 17),
	(67, 54, 16),
	(68, 54, 17),
	(71, 56, 16),
	(72, 56, 17),
	(73, 57, 16),
	(74, 57, 17),
	(77, 59, 16),
	(78, 59, 17),
	(79, 60, 16),
	(80, 60, 17),
	(81, 61, 16),
	(82, 61, 20),
	(83, 61, 17),
	(84, 62, 16),
	(85, 62, 20),
	(86, 62, 17),
	(87, 63, 16),
	(88, 63, 20),
	(89, 63, 17),
	(90, 64, 16),
	(91, 64, 20),
	(92, 64, 17);
/*!40000 ALTER TABLE `contests_jurors` ENABLE KEYS */;

-- Дъмп данни за таблица photo_contest.contest_invited_junkies: ~11 rows (приблизително)
/*!40000 ALTER TABLE `contest_invited_junkies` DISABLE KEYS */;
INSERT INTO `contest_invited_junkies` (`id`, `contest_id`, `user_id`) VALUES
	(17, 59, 27),
	(18, 59, 19),
	(19, 59, 21),
	(20, 59, 29),
	(21, 59, 25),
	(22, 59, 28),
	(23, 59, 20),
	(24, 59, 26),
	(25, 59, 23),
	(26, 64, 19),
	(27, 64, 26);
/*!40000 ALTER TABLE `contest_invited_junkies` ENABLE KEYS */;

-- Дъмп данни за таблица photo_contest.contest_participants: ~41 rows (приблизително)
/*!40000 ALTER TABLE `contest_participants` DISABLE KEYS */;
INSERT INTO `contest_participants` (`id`, `contest_id`, `user_id`) VALUES
	(26, 49, 18),
	(27, 49, 19),
	(28, 49, 20),
	(29, 49, 21),
	(30, 49, 22),
	(31, 50, 20),
	(32, 50, 19),
	(33, 51, 20),
	(34, 51, 22),
	(35, 51, 23),
	(36, 53, 18),
	(37, 53, 19),
	(38, 53, 25),
	(39, 54, 20),
	(40, 54, 28),
	(41, 54, 27),
	(43, 56, 20),
	(44, 56, 26),
	(45, 56, 29),
	(46, 56, 25),
	(47, 57, 20),
	(48, 57, 24),
	(49, 57, 22),
	(50, 59, 19),
	(51, 59, 21),
	(52, 59, 26),
	(53, 60, 20),
	(54, 60, 19),
	(55, 61, 18),
	(56, 61, 19),
	(57, 61, 21),
	(58, 61, 22),
	(59, 61, 23),
	(60, 61, 24),
	(61, 61, 25),
	(62, 62, 23),
	(63, 62, 26),
	(64, 63, 23),
	(65, 63, 26),
	(66, 64, 19),
	(67, 64, 26);
/*!40000 ALTER TABLE `contest_participants` ENABLE KEYS */;

-- Дъмп данни за таблица photo_contest.contest_winners: ~27 rows (приблизително)
/*!40000 ALTER TABLE `contest_winners` DISABLE KEYS */;
INSERT INTO `contest_winners` (`id`, `contest_id`, `image_id`, `place`) VALUES
	(7, 49, 91, 1),
	(8, 49, 92, 2),
	(9, 49, 90, 3),
	(10, 50, 94, 1),
	(11, 50, 95, 2),
	(12, 51, 96, 1),
	(13, 51, 98, 3),
	(14, 53, 99, 1),
	(15, 54, 102, 1),
	(16, 54, 104, 2),
	(17, 54, 103, 3),
	(18, 56, 107, 1),
	(19, 56, 105, 2),
	(20, 56, 108, 3),
	(21, 57, 109, 1),
	(22, 57, 111, 2),
	(23, 57, 110, 3),
	(24, 59, 114, 1),
	(25, 59, 112, 2),
	(26, 59, 113, 3),
	(27, 60, 115, 1),
	(28, 60, 116, 2),
	(29, 61, 120, 1),
	(30, 61, 117, 2),
	(31, 61, 119, 3),
	(32, 64, 128, 1),
	(33, 64, 129, 2);
/*!40000 ALTER TABLE `contest_winners` ENABLE KEYS */;

-- Дъмп данни за таблица photo_contest.images: ~41 rows (приблизително)
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` (`id`, `image`, `user_id`, `title`, `story`, `contest_id`, `total_score`) VALUES
	(89, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\Thinking-of-getting-a-cat.png', 18, 'My lovely cat', 'Yesterday I got back home from work and saw my cat staring at me like that. It\'s so cute!!', 49, 9),
	(90, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\cat-mad_13.jpg', 19, 'Angry Looks', 'I don\'t know what it is with my cat, but it always looks me like that.', 49, 13),
	(91, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\image-31454-800.jpg', 20, 'Pure Cuteness', 'I just captured that and I\'m so excited. It looks way too cute.', 49, 20),
	(92, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\why-cats-are-best-pets-1559241235.jpg', 21, 'White & Pink', 'Went out with my cat and travelled 3 hours to get to that place, but it was worth all along.', 49, 15),
	(93, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\young-birman-kitten-playing-royalty-free-image-1580488897.jpg', 22, 'Playing', 'I saw my cat playing with some toys and it looked so nice.', 49, 10),
	(94, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\worlds-smallest-dogs-1647336453.jpg', 20, 'My puppy', 'One of the best photos that I was able to take of my doggy.', 50, 13),
	(95, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\golder-retriever-puppy.jpeg', 19, 'My puppy looking at me.', 'It\'s so cute and I love it so much.', 50, 6),
	(96, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\4288.jpg', 20, 'Lil bird', 'Found this bird today after my morning coffee.', 51, 13),
	(97, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\https___cdn.cnn.com_cnnnext_dam_assets_210316134609-01-wisdom-project-spring.jpg', 22, 'Season of hope', 'The forest is so incredible.', 51, 6),
	(98, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\spring-26.jpg', 23, 'Birds in spring.', 'Captured this on my morning walk.', 51, 6),
	(99, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\9cd8350a4df0afe73191d4280286d7ee.jpg', 18, 'Beautiful Bears', 'This looks so nice.', 53, 15),
	(100, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\wp2174955.jpg', 19, 'Tiger', 'A big cat being hungry and looking at it\'s next meal.', 53, 15),
	(101, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\best_wild_animals_photography_by_roberttdixon_dc5xvm0-fullview.jpg', 25, 'Zebras', 'Zebras running in the wild.', 53, 11),
	(102, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\1048232144.jpg.jpg', 20, 'Sunset', 'Got this shot last summer and I\'m really proud with it.', 54, 13),
	(103, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\CqOvBFE.jpg', 28, 'Sunny day', 'Being there made my day.', 54, 3),
	(104, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\20210622-torontosunset1.jpg', 27, 'Red sunset', 'A beatiful shot of my town at 7pm.', 54, 12),
	(105, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\mountain-wallpaper-whatspaper-5.jpg', 20, 'Local Mountain', '3 months ago I captured this beauty.', 56, 10),
	(106, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\photo-1454496522488-7a8e488e8606.jpg', 26, 'Winter mountains', 'That\'s why I love winter as much as I love summer.', 56, 13),
	(107, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\wallpapersden.com_cloudy-mountains_3840x2160.jpg', 29, 'Above the sky', 'Had to fly yesterday, so there you go.', 56, 13),
	(108, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\rocky-mountain-wallpapers-for-desktop-hd-free.jpg', 25, 'Summer mountains', 'A great view to all surrounding mountains.', 56, 9),
	(109, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\1434077.jpg', 20, 'My cat', 'Nice shot of my cat.', 57, 13),
	(110, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\ws_Cute_Cat_1920x1200.jpg', 24, 'Killer looks', 'My cat was so curious at the camera.', 57, 11),
	(111, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\cat-1-4K.jpg', 22, 'Hiding again', 'I wonder what is in his head.', 57, 12),
	(112, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\best-pet-rabbit-breeds.png', 19, 'My little rabbit', 'Little little rabbit.', 59, 12),
	(113, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\best-pet-rabbit-breeds.jpg', 21, '2 cute rabbits', 'I have this for 2 weeks now and I\'m so happy', 59, 10),
	(114, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\0008194188ca2f939b39884981bdedd0.jpg', 26, 'Chinchilla', 'A lovely chinchilla I\'ve got living with me.', 59, 13),
	(115, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\Summer-Sun-_-Hyams-Beach-V1.3.jpg', 20, 'Blue sea', 'Just a capture of the clear sea.', 60, 13),
	(116, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\beach-2836300__340.jpg', 19, 'Seashore', 'It looks so great doesn\'t it', 60, 9),
	(117, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\wp5300024.jpg', 18, 'Autumn forest', 'This thing looks so good.', 61, 18),
	(118, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\qwgKhjq.jpg', 19, 'Over the top', 'Above the world. And its amazing.', 61, 13),
	(119, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\Qjvt9d.jpg', 21, 'Autumn lake', 'I think the reflection adds a lot to the capture.', 61, 16),
	(120, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\Free-Road-Autumn-Wallpapers-HD-1080.jpg', 22, 'Railway', 'A good day to travel.', 61, 30),
	(121, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\leaf-autumn-wallpapers-42045-684767-2378028.png', 23, 'A leaf', 'Just a good looking leaf in the autumn can make your day.', 61, 15),
	(122, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\Autumn-wallpapers-for-mobiles.jpg', 24, 'My villa', 'The season colors fit the villa don\'t they.', 61, 15),
	(123, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\281-2813959_autumn-wallpapers-free-autumn-desktop-wallpaper-desktop-backgrounds.jpg', 25, 'Leaves', 'Vibing in october.', 61, 12),
	(124, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\justin-shen-faN06Gc1huM-unsplash.jpg', 23, 'My best dunk', 'This is a picture of me making my best ever dunk. I won first place in a contest with theh full 50 points.', 62, 0),
	(125, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\colin-lloyd-LOh-9PgdPZw-unsplash.jpg', 26, 'Snowboarding in the wild', 'We decided to go out and snowboard for the day. This is the best image of the day!', 62, 0),
	(126, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\webaliser-_TPTXZd9mOo-unsplash.jpg', 23, 'I\'m in love with this house!', 'This is my dream house. It isn\'t mine but one day it will be! ', 63, 0),
	(127, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\r-architecture-KQPEhYweLrQ-unsplash.jpg', 26, 'Simple yet beautiful', 'I saw this house on my way to the suburbs and asked the owner if I could take a photo of it. \r\nHe said yes. He even invited me for a coffee.', 63, 0),
	(128, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\par17512-teaser-story-big.jpg', 19, 'Ainstein', 'Most know picture of Ainstein. ', 64, 23),
	(129, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\stock-photo-88195029.jpg', 26, 'Blue eyed ginger girl', 'Girnger is beutiful', 64, 13);
/*!40000 ALTER TABLE `images` ENABLE KEYS */;

-- Дъмп данни за таблица photo_contest.images_reviews: ~45 rows (приблизително)
/*!40000 ALTER TABLE `images_reviews` DISABLE KEYS */;
INSERT INTO `images_reviews` (`id`, `image_id`, `review_id`) VALUES
	(22, 90, 23),
	(23, 91, 24),
	(24, 89, 25),
	(25, 93, 26),
	(26, 92, 27),
	(27, 91, 28),
	(28, 92, 29),
	(29, 93, 30),
	(30, 89, 31),
	(31, 90, 32),
	(32, 94, 33),
	(33, 96, 34),
	(34, 100, 35),
	(35, 99, 36),
	(36, 101, 37),
	(37, 103, 38),
	(38, 102, 39),
	(39, 104, 40),
	(40, 106, 41),
	(41, 105, 42),
	(42, 108, 43),
	(43, 107, 44),
	(44, 111, 45),
	(45, 110, 46),
	(46, 109, 47),
	(47, 114, 48),
	(48, 112, 49),
	(49, 113, 50),
	(50, 115, 51),
	(51, 116, 52),
	(52, 118, 53),
	(53, 120, 54),
	(54, 122, 55),
	(55, 121, 56),
	(56, 122, 57),
	(57, 120, 58),
	(58, 117, 59),
	(59, 120, 60),
	(60, 119, 61),
	(61, 123, 62),
	(62, 117, 63),
	(63, 128, 64),
	(64, 129, 65),
	(65, 128, 66),
	(66, 129, 67);
/*!40000 ALTER TABLE `images_reviews` ENABLE KEYS */;

-- Дъмп данни за таблица photo_contest.ranks: ~4 rows (приблизително)
/*!40000 ALTER TABLE `ranks` DISABLE KEYS */;
INSERT INTO `ranks` (`id`, `name`) VALUES
	(1, 'Junkie'),
	(2, 'Enthusiast'),
	(3, 'Master'),
	(4, 'Wise and Benelovent Photo Dictator');
/*!40000 ALTER TABLE `ranks` ENABLE KEYS */;

-- Дъмп данни за таблица photo_contest.review: ~45 rows (приблизително)
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` (`id`, `score`, `comment`, `juror_id`) VALUES
	(23, 6, 'I like the grumpy looks', 16),
	(24, 10, 'Absolute killer photo', 16),
	(25, 4, 'It looks nice', 16),
	(26, 4, 'Really good looking cat', 16),
	(27, 8, 'Very good photo.', 16),
	(28, 10, 'It\'s so cute', 17),
	(29, 7, 'Indeed the travel was worth all along.', 17),
	(30, 6, 'The toy looks as cute as the cat.', 17),
	(31, 5, 'It\'s so nice.', 17),
	(32, 7, 'Gruuumpy.. My cat gives me similar looks in the mornings.', 17),
	(33, 10, 'That\'s a really good picture', 16),
	(34, 10, 'Looks incredible', 16),
	(35, 9, 'Incredible capture this is.', 20),
	(36, 9, 'The bears are so cute.', 20),
	(37, 5, 'This looks so nice.', 20),
	(38, 0, 'The image is not in the right category!', 16),
	(39, 10, 'It looks good indeed.', 16),
	(40, 9, 'Looks so nice!', 16),
	(41, 10, '10 for how massive it is.', 16),
	(42, 7, 'Looks really good.', 16),
	(43, 6, 'I do quite like it.', 16),
	(44, 10, 'Must\'ve been a good flight.', 16),
	(45, 9, 'Looks cute.', 16),
	(46, 8, 'Nice look it gave you!', 16),
	(47, 10, 'It looks so good, I cannot rate it less than 10.', 16),
	(48, 10, 'These animals are so cute.', 17),
	(49, 9, 'What a cute rabbit this is', 17),
	(50, 7, 'Nice cuties', 17),
	(51, 10, 'Looks soo good', 16),
	(52, 6, 'It is nice indeed.', 16),
	(53, 7, 'A good one', 20),
	(54, 10, 'Really good looking photo.', 20),
	(55, 7, 'The villa is lovely.', 20),
	(56, 9, 'It looks so nice!', 20),
	(57, 5, 'Good looking capture.', 17),
	(58, 10, 'Really nice capture', 17),
	(59, 6, 'Nice one!', 17),
	(60, 10, 'Great photo!!', 16),
	(61, 10, 'The lake makes it better indeed.', 16),
	(62, 6, 'It looks good.', 16),
	(63, 9, 'Nice pathway', 16),
	(64, 10, 'Love it, he is incredible!', 16),
	(65, 5, 'Beautiful image!', 16),
	(66, 10, 'Best shot I\'ve seen lately', 20),
	(67, 5, 'She is amazing!', 20);
/*!40000 ALTER TABLE `review` ENABLE KEYS */;

-- Дъмп данни за таблица photo_contest.scores: ~0 rows (приблизително)
/*!40000 ALTER TABLE `scores` DISABLE KEYS */;
/*!40000 ALTER TABLE `scores` ENABLE KEYS */;

-- Дъмп данни за таблица photo_contest.users: ~14 rows (приблизително)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `first_name`, `last_name`, `password`, `is_organiser`, `rank_id`, `points`, `email`, `is_deleted`, `reset_password_token`, `avatar`) VALUES
	(16, 'Jamy', 'James', 'Johnson', 'johnson1', 1, 1, -1, 'jamyjohnson551236@gmail.com', 0, NULL, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\avatars\\86289.jpg'),
	(17, 'Willy', 'William', 'Smith', 'willy1', 1, 1, -1, 'willlly15552@gmail.com', 0, NULL, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\avatars\\tiger.png'),
	(18, 'Marrri', 'Maria', 'Martinez', 'mary14', 0, 2, 78, 'marry1316213@gmail.com', 0, NULL, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\default_avatar\\default_avatar.jpg'),
	(19, 'MickeyMick', 'Michael', 'Davis', 'mickey1', 0, 3, 186, 'mickey616161@gmail.com', 0, NULL, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\avatars\\png_mickey_mouse_75950.png'),
	(20, 'JennyGoddess', 'Jennifer', 'Clark', 'jenny1', 0, 3, 392, 'jenny16623@gmail.com', 0, NULL, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\avatars\\72846.png'),
	(21, 'Dondy', 'Donald', 'Young', 'donald1', 0, 2, 80, 'dondy515123@gmail.com', 0, NULL, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\avatars\\unnamed.png'),
	(22, 'TomTom', 'Thomas', 'Harris', 'thomas1', 0, 2, 89, 'thomasth3gr8123123@gmail.com', 0, NULL, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\avatars\\tom-5158824_960_720 (1).jpeg'),
	(23, 'Richykid', 'Richard', 'Wright', 'richard1', 0, 1, 22, 'rich52wright351@gmail.com', 0, NULL, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\avatars\\Richard-Branson_bestsellers.jpg'),
	(24, 'Sussy', 'Susan', 'Flores', 'susan1', 0, 1, 22, 'su55yfl0rees@gmail.com', 0, NULL, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\avatars\\photo-1615022702095-ff2c036f3360.jpg'),
	(25, 'Charlyyyies', 'Charles', 'Thompson', 'charles1', 0, 1, 23, 'charllliessstompison@gmail.com', 0, NULL, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\default_avatar\\default_avatar.jpg'),
	(26, 'Sandie', 'Sandra', 'Adams', 'sandra1', 0, 2, 132, 'sandamsluv@gmail.com', 0, NULL, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\default_avatar\\default_avatar.jpg'),
	(27, 'Anthonnny', 'Anthony', 'Baker', 'anthony1', 0, 1, 36, 'anthonybkr424@gmail.com', 0, NULL, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\avatars\\AnthonyHopkins10TIFF.jpg'),
	(28, 'Kimbyby', 'Kimberly', 'Rivera', 'kimberly1', 0, 1, 21, 'kimriiivv@gmail.com', 1, NULL, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\default_avatar\\default_avatar.jpg'),
	(29, 'NickityTickity', 'Nicholas', 'Hall', 'nicholas1', 0, 1, 41, 'nickhallaf@gmail.com', 0, NULL, 'C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\default_avatar\\default_avatar.jpg');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
