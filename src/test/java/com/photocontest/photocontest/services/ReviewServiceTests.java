package com.photocontest.photocontest.services;

import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.*;
import com.photocontest.repositories.contracts.ContestRepository;
import com.photocontest.repositories.contracts.ImageRepository;
import com.photocontest.repositories.contracts.ImagesReviewsRepository;
import com.photocontest.repositories.contracts.ReviewRepository;
import com.photocontest.services.ReviewServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static com.photocontest.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ReviewServiceTests {

    @Mock
    ContestRepository contestRepository;

    @Mock
    ImageRepository imageRepository;

    @Mock
    ReviewRepository reviewRepository;

    @Mock
    ImagesReviewsRepository imagesReviewsRepository;

    @InjectMocks
    ReviewServiceImpl reviewService;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(reviewRepository.getAll())
                .thenReturn(new ArrayList<>());

        reviewService.getAll();

        Mockito.verify(reviewRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_should_returnReview_when_matchExists() {
        Review mockReview = createMockReview();
        Mockito.when(reviewRepository.getById(mockReview.getId()))
                .thenReturn(mockReview);
        Review takenReview = reviewService.getById(mockReview.getId());

        Assertions.assertEquals(mockReview.getId(), takenReview.getId());
    }

    @Test
    public void reviewImage_should_throw_UnauthorizedOperationException() {
        Contest contest = createMockContest();
        Image image = createMockImage();
        User user = createMockOrganizer();
        contest.setJurry(Set.of(user));
        image.setReviews(Set.of(createMockReview()));
        Review review = createMockReview();

        Mockito.when(contestRepository.getById(contest.getId()))
                .thenReturn(contest);
        Mockito.when(imageRepository.getById(image.getId()))
                .thenReturn(image);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> reviewService.reviewImage(review, 1, user, 1));
    }

    @Test
    public void reviewImage_should_throw_UnauthorizedOperationException_whenUserIsNotJuror() {
        Contest contest = createMockContest();
        Image image = createMockImage();
        User user = createMockUser();
        Review review = createMockReview();

        Mockito.when(contestRepository.getById(contest.getId()))
                .thenReturn(contest);
        Mockito.when(imageRepository.getById(image.getId()))
                .thenReturn(image);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> reviewService.reviewImage(review, 1, user, 1));
    }

    @Test
    public void reviewImage_should_addReviewToImage() {
        Contest contest = createMockContest();
        Image image = createMockImage();
        User user = createMockOrganizer();
        contest.setJurry(Set.of(user));
        Review review = createMockReview();

        Mockito.when(contestRepository.getById(contest.getId()))
                .thenReturn(contest);
        Mockito.when(imageRepository.getById(image.getId()))
                .thenReturn(image);

        reviewService.reviewImage(review, 1, user, 1);
        Mockito.verify(reviewRepository, Mockito.times(1))
                .create(review);
    }
}



