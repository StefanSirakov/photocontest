package com.photocontest.photocontest.services;

import com.photocontest.models.User;
import com.photocontest.models.UserRank;
import com.photocontest.repositories.contracts.UserRankRepository;
import com.photocontest.services.UserRankServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.photocontest.photocontest.Helpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class UserRankServiceTests {

    @Mock
    UserRankRepository mockRepository;

    @InjectMocks
    UserRankServiceImpl userRankService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        userRankService.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_should_callRepository() {
        Mockito.when(mockRepository.getById(1))
                .thenReturn(new UserRank());

        userRankService.getById(1);

        Mockito.verify(mockRepository, Mockito.times(1)).getById(1);

    }

}
