package com.photocontest.photocontest.services;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.Category;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.CategoryRepository;
import com.photocontest.services.CategoryServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.photocontest.photocontest.Helpers.createMockCategory;
import static com.photocontest.photocontest.Helpers.createMockUser;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTests {
    
    @Mock
    CategoryRepository categoryRepository;

    @InjectMocks
    CategoryServiceImpl categoryService;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(categoryRepository.getAll())
                .thenReturn(new ArrayList<>());

        categoryService.getAll();

        Mockito.verify(categoryRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_should_returnCategory_when_matchExists() {
        Category mockCategory = createMockCategory();
        Mockito.when(categoryRepository.getById(mockCategory.getId()))
                .thenReturn(mockCategory);
        Category takenCategory = categoryService.getById(mockCategory.getId());

        Assertions.assertEquals(mockCategory.getName(), takenCategory.getName());
    }

    @Test
    public void getByName_should_returnCategory_when_matchExists() {
        Category mockCategory = createMockCategory();
        Mockito.when(categoryRepository.getByField(anyString(), anyString()))
                .thenReturn(mockCategory);

        Category result = categoryService.getByName(mockCategory.getName());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCategory.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCategory.getName(), result.getName())
        );
    }

    @Test
    public void delete_should_deleteCategory() {
        Category mockCategory = createMockCategory();
        User user = createMockUser();

        categoryService.delete(mockCategory.getId(), user);

        Mockito.verify(categoryRepository, Mockito.times(1))
                .delete(mockCategory.getId());
    }

    @Test
    public void search_should_callRepository() {
        List<Category> categoryList = new ArrayList<>();
        Mockito.when(categoryRepository.search(Optional.empty()))
            .thenReturn(categoryList);

        categoryService.search(Optional.empty());

        Mockito.verify(categoryRepository, Mockito.times(1))
                .search(Optional.empty());

    }

}
