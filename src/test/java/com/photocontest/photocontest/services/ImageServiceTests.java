package com.photocontest.photocontest.services;

import com.photocontest.exceptions.DuplicateEntityException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.Contest;
import com.photocontest.models.ContestParticipants;
import com.photocontest.models.Image;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.ContestParticipantsRepository;
import com.photocontest.repositories.contracts.ImageRepository;
import com.photocontest.repositories.contracts.UserRepository;
import com.photocontest.services.ImageServiceImpl;
import com.photocontest.services.UserServiceImpl;
import com.photocontest.services.contracts.ContestService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import static com.photocontest.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ImageServiceTests {

    @Mock
    ImageRepository mockRepository;

    @Mock
    ContestParticipantsRepository contestParticipantsRepository;

    @Mock
    ContestService contestService;

    @InjectMocks
    ImageServiceImpl imageService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        imageService.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_should_returnImage_when_matchExists() {
        Image image = createMockImage();
        Mockito.when(mockRepository.getById(image.getId()))
                .thenReturn(image);
        Image takenImage = imageService.getById(image.getId());

        Assertions.assertEquals(image.getTitle(), takenImage.getTitle());
    }

    @Test
    public void getByUserId_should_callRepository() {
        Mockito.when(mockRepository.getByUserId(1))
                .thenReturn(new ArrayList<>());

        imageService.getByUserId(1);

        Mockito.verify(mockRepository, Mockito.times(1)).getByUserId(1);
    }

    @Test
    public void sortByScore_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        imageService.sortByScore();

        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void create_should_callRepository_when_validInfo () {
        Contest contest = createMockContest();
        Image image = createMockImage();

        Mockito.when(contestService.getById(contest.getId()))
                .thenReturn(contest);

        imageService.create(image, contest.getId());

        Mockito.verify(mockRepository, Mockito.times(1)).create(image);
    }

    @Test
    public void search_should_throw_when_notAuthorized() {
        User user = createMockUser();
        Contest contest = createMockContest();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> imageService.searchPhoto(Optional.empty(),user,contest.getId()));
    }

    @Test
    public void search_should_callRepository_when_authorized() {
        User user = createMockOrganizer();
        Contest contest = createMockContest();

        imageService.searchPhoto(Optional.empty(),user, contest.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .search(Optional.empty(),contest.getId());
    }

    @Test
    public void delete_should_throw_when_notAuthorized() {
        User user = createMockUser();
        Contest contest = createMockContest();
        Image image = createMockImage();

        Mockito.when(contestService.getById(contest.getId()))
                .thenReturn(contest);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> imageService.delete(contest.getId(),image.getId(),user));
    }

    @Test
    public void delete_should_callRepository_whenValid() {
        User user = createMockUser();
        Contest contest = createMockContest();
        Image image = createMockImage();
        contest.setJurry(Set.of(user));

        Mockito.when(contestService.getById(contest.getId()))
                .thenReturn(contest);

        imageService.delete(contest.getId(), image.getId(), user);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(image.getId());

    }

}
