package com.photocontest.photocontest.services;

import com.photocontest.exceptions.DuplicateEntityException;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.*;
import com.photocontest.models.daos.ImageEvaluationDao;
import com.photocontest.repositories.contracts.*;
import com.photocontest.services.CategoryServiceImpl;
import com.photocontest.services.ContestServiceImpl;
import com.photocontest.services.contracts.CategoryService;
import com.photocontest.services.contracts.ContestService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.photocontest.photocontest.Helpers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class ContestServiceTests {

    @Mock
    ContestRepository contestRepository;
    @Mock
    UserRepository userRepository;
    @Mock
    ImageRepository imageRepository;
    @Mock
    CategoryRepository categoryRepository;
    @Mock
    ContestInvitedJunkieRepository contestInvitedJunkieRepository;
    @Mock
    ContestCategoryRepository contestCategoryRepository;
    @Mock
    ContestParticipantsRepository contestParticipantsRepository;
    @Mock
    ContestsJurorsRepository contestsJurorsRepository;
    @Mock
    ContestWinnerRepostitory contestWinnerRepostitory;
    @Mock
    UserRankRepository userRankRepository;
    @InjectMocks
    ContestServiceImpl contestService;
    @InjectMocks
    CategoryServiceImpl categoryService;


    @Test
    public void getAll_should_callRepository() {
        Mockito.when(contestRepository.getAll())
                .thenReturn(new ArrayList<>());

        contestService.getAll();

        Mockito.verify(contestRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_should_returnContest_when_matchExists() {
        Contest mockContest = createMockContest();
        Mockito.when(contestRepository.getById(mockContest.getId()))
                .thenReturn(mockContest);
        Contest takenContest = contestService.getById(mockContest.getId());

        Assertions.assertEquals(mockContest.getId(), takenContest.getId());
    }

    @Test
    public void getByUserEntry_should_returnContest_when_matchExists() {
        Contest mockContest = createMockContest();
        Mockito.when(contestParticipantsRepository.getByUserEntry(1))
                .thenReturn(List.of(createMockContestParticipants()));

        Mockito.when(contestRepository.getById(1))
                .thenReturn(mockContest);

        contestService.getByUserEntry(1);

        Mockito.verify(contestRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void getByUsername_should_returnContest_when_matchExists() {
        Contest mockContest = createMockContest();
        User user = createMockUser();
        Mockito.when(contestRepository.getByField(anyString(), anyString()))
                .thenReturn(mockContest);
        Contest takenContest = contestService.getByUsername(user.getUsername());

        Assertions.assertEquals(mockContest.getId(), takenContest.getId());
    }

    @Test
    public void create_should_throwUnauthorizedOperationException_when_userIsNotOrganizer() {
        Contest mockContest = createMockContest();
        User user = createMockUser();

        contestRepository.create(mockContest);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> contestService.create(mockContest,"category", user));
    }

    @Test
    public void create_should_throwDuplicateEntityException_when_contestWithSameTitleExists() {
        Contest mockContest = createMockContest();
        User user = createMockOrganizer();

        Mockito.when(contestRepository.getByField(anyString(), anyString()))
                .thenThrow(DuplicateEntityException.class);

        contestRepository.create(mockContest);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> contestService.create(mockContest, "category", user));
    }

    @Test
    public void create_should_createContest() {
        Contest mockContest = createMockContest();
        User user = createMockOrganizer();

        Mockito.when(contestRepository.getByField(anyString(), anyString()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(categoryRepository.getByField(anyString(), anyString()))
                .thenReturn(createMockCategory());

        contestService.create(mockContest, "category", user);

        Mockito.verify(contestRepository, Mockito.times(1))
                .create(mockContest);
    }

    @Test
    public void update_should_throwUnathorizedException_when_userIsNotOrganizer() {
        Contest mockContest = createMockContest();
        User mockUser = createMockUser();

        contestRepository.update(mockContest);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> contestService.update(mockContest, mockUser));
    }

    @Test
    public void update_should_throwDuplicateEntityException_when_contestNameIsTaken() {
        Contest mockContest = createMockContest();
        mockContest.setTitle("test-name");
        Contest anotherMockContest = createMockContest();
        anotherMockContest.setId(2);
        anotherMockContest.setTitle("test-name");

        Mockito.when(contestRepository.getByField(anyString(), anyString()))
                .thenReturn(anotherMockContest);

        contestRepository.update(mockContest);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> contestService.update(mockContest, createMockOrganizer()));
    }

    @Test
    public void update_should_callRepository() {
        Contest mockContest = createMockContest();

        Mockito.when(contestRepository.getByField(anyString(), anyString()))
                .thenThrow(EntityNotFoundException.class);

        contestService.update(mockContest, createMockOrganizer());

        Mockito.verify(contestRepository, Mockito.times(1))
                .update(mockContest);
    }

    @Test
    void delete_should_throwUnauthorizedException_when_userIsNotOrganizer() {
        Contest mockContest = createMockContest();
        User mockUser = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> contestService.delete(mockContest.getId(), mockUser));
    }

    @Test
    void delete_should_callRepository_when_userIsOrganizer() {
        Contest mockContest = createMockContest();
        User mockUser = createMockOrganizer();

        contestService.delete(mockContest.getId(), mockUser);

        Mockito.verify(contestRepository, Mockito.times(1))
                .delete(mockContest.getId());
    }

    @Test
    public void search_should_callRepository() {
        List<Contest> contestList = new ArrayList<>();
        Mockito.when(contestRepository.search(Optional.empty()))
                .thenReturn(contestList);

        contestService.search(Optional.empty());

        Mockito.verify(contestRepository, Mockito.times(1))
                .search(Optional.empty());

    }

    @Test
    public void filter_should_callRepository() {
        contestService.filter(Optional.empty(), Optional.empty(), Optional.empty());

        Mockito.verify(contestRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Test
    public void filter_should_callRepository_when_phaseOne() {
        contestService.filter(Optional.empty(), Optional.empty(), Optional.of("Phase I"));

        Mockito.verify(contestRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty(), Optional.of(1));
    }

    @Test
    public void filter_should_callRepository_when_phaseTwo() {
        contestService.filter(Optional.empty(), Optional.empty(), Optional.of("Phase II"));

        Mockito.verify(contestRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty(), Optional.of(2));
    }

    @Test
    public void filter_should_callRepository_when_phaseThree() {
        contestService.filter(Optional.empty(), Optional.empty(), Optional.of("Finished"));

        Mockito.verify(contestRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty(), Optional.of(3));
    }

    @Test
    public void checkPhase__should_changePhase() {
        List<Contest> contestList = new ArrayList<>();
        Contest mockContest = createMockContest();
        contestList.add(mockContest);
        mockContest.setPhaseTwoStartingTime(LocalDateTime.now().minusHours(10));
        Mockito.when(contestRepository.getAll()).thenReturn(contestList);

        contestService.checkPhase();

        Mockito.verify(contestRepository, (Mockito.times(1)))
                .update(mockContest);
    }

    @Test
    public void checkPhase__should_endContest() {
        List<Contest> contestList = new ArrayList<>();
        Contest mockContest = createMockContest();
        contestList.add(mockContest);
        mockContest.setPhase(2);
        mockContest.setContestEndingTime(LocalDateTime.now().minusHours(10));
        Mockito.when(contestRepository.getAll()).thenReturn(contestList);

        contestService.checkPhase();

        Mockito.verify(contestRepository, (Mockito.times(1)))
                .update(mockContest);
    }

    @Test
    public void updateUserRanks_should_callUserRepository() {
        List<User> users = new ArrayList<>();
        User user = createMockUser();
        user.setPoints(60);
        user.setUserRank(createMockUserRank());

        Contest mockContest = createMockContest();
        mockContest.setParticipants(Set.of(user));
        users.add(user);
        UserRank userRank = createMockUserRank();

        Mockito.when(contestRepository.getAll()).thenReturn(List.of(mockContest));
        mockContest.setContestEndingTime(LocalDateTime.now().minusHours(10));
        mockContest.setPhase(2);

        Mockito.when(userRankRepository.getById(user.getUserRank().getId() + 1))
                .thenReturn(userRank);

        contestService.checkPhase();

        Mockito.verify(userRepository, Mockito.times(1))
                .update(user);

    }

    @Test
    public void addPointsForParticipating_should_addPointsForOpenContests() {
        List<Contest> contestList = new ArrayList<>();
        Contest mockContest = createMockContest();
        contestList.add(mockContest);
        mockContest.setPhase(2);
        mockContest.setImages(Set.of(createMockImage()));
        mockContest.setContestEndingTime(LocalDateTime.now().minusHours(10));
        User user = createMockUser();
        user.setUserRank(createMockUserRank());
        user.setPoints(20);
        mockContest.setParticipants(Set.of(user));

        Mockito.when(contestRepository.getAll()).thenReturn(contestList);

        contestService.checkPhase();

        Mockito.verify(userRepository, Mockito.times(2))
                .update(user);


    }
    @Test
    public void addPointsForParticipating_should_addPointsForInvitationalContests() {
        List<Contest> contestList = new ArrayList<>();
        Contest mockContest = createMockContest();
        contestList.add(mockContest);
        mockContest.setPhase(2);
        mockContest.setImages(Set.of(createMockImage()));
        mockContest.setIsOpen(false);
        mockContest.setContestEndingTime(LocalDateTime.now().minusHours(10));
        User user = createMockUser();
        user.setUserRank(createMockUserRank());
        user.setPoints(20);
        mockContest.setParticipants(Set.of(user));

        Mockito.when(contestRepository.getAll()).thenReturn(contestList);

        contestService.checkPhase();

        Mockito.verify(userRepository, Mockito.times(2))
                .update(user);
    }

    @Test
    public void distributeSecondAndThirdPlacePoints_should_returnCorrectIndex() {
        List<Contest> contestList = new ArrayList<>();
        Contest mockContest = createMockContest();
        contestList.add(mockContest);
        mockContest.setPhase(2);
        Image image = createMockImage();
        image.setTitle("titleImage");
        image.setTotalScore(30);
        mockContest.setImages(Set.of(createMockImage(), image));
        mockContest.setIsOpen(false);
        mockContest.setContestEndingTime(LocalDateTime.now().minusHours(10));
        User user = createMockUser();
        user.setUserRank(createMockUserRank());
        user.setPoints(20);
        mockContest.setParticipants(Set.of(user));

        Mockito.when(contestRepository.getAll()).thenReturn(contestList);
        contestService.checkPhase();

        Mockito.verify(contestWinnerRepostitory, Mockito.times(2))
                .create(Mockito.any(ContestWinner.class));

    }

    @Test
    public void distributeSecondAndThirdPlacePoints_should_returnCorrectIndex2() {
        List<Contest> contestList = new ArrayList<>();
        Contest mockContest = createMockContest();
        contestList.add(mockContest);
        mockContest.setPhase(2);
        Image image = createMockImage();
        image.setTitle("titleImage");
        image.setTotalScore(30);
        Image image2 = createMockImage();
        image.setTitle("titleImage2");
        image.setTotalScore(40);
        mockContest.setImages(Set.of(createMockImage(), image, image2));
        mockContest.setIsOpen(false);
        mockContest.setContestEndingTime(LocalDateTime.now().minusHours(10));
        User user = createMockUser();
        user.setUserRank(createMockUserRank());
        user.setPoints(20);
        mockContest.setParticipants(Set.of(user));

        Mockito.when(contestRepository.getAll()).thenReturn(contestList);
        contestService.checkPhase();

        Mockito.verify(contestWinnerRepostitory, Mockito.times(2))
                .create(Mockito.any(ContestWinner.class));

    }

    @Test
    public void distributeSecondAndThirdPlacePoints_should_returnCorrectIndex3() {
        List<Contest> contestList = new ArrayList<>();
        Contest mockContest = createMockContest();
        contestList.add(mockContest);
        mockContest.setPhase(2);
        Image image = createMockImage();
        image.setTitle("titleImage");
        image.setTotalScore(15);
        Image image2 = createMockImage();
        image2.setTitle("titleImage2");
        image2.setTotalScore(20);
        mockContest.setImages(Set.of(createMockImage(), image, image2));
        mockContest.setIsOpen(false);
        mockContest.setContestEndingTime(LocalDateTime.now().minusHours(10));
        User user = createMockUser();
        user.setUserRank(createMockUserRank());
        user.setPoints(20);
        mockContest.setParticipants(Set.of(user));

        Mockito.when(contestRepository.getAll()).thenReturn(contestList);
        contestService.checkPhase();

        Mockito.verify(contestWinnerRepostitory, Mockito.times(3))
                .create(Mockito.any(ContestWinner.class));

    }

    @Test
    public void distributeSecondAndThirdPlacePoints_should_returnCorrectIndex4() {
        List<Contest> contestList = new ArrayList<>();
        Contest mockContest = createMockContest();
        contestList.add(mockContest);
        mockContest.setPhase(2);
        Image image = createMockImage();
        image.setTitle("titleImage");
        image.setTotalScore(20);
        Image image2 = createMockImage();
        image2.setTitle("titleImage2");
        image2.setTotalScore(20);
        Image image3 = createMockImage();
        image3.setTitle("titleImage2");
        image3.setTotalScore(20);
        mockContest.setImages(Set.of(createMockImage(), image, image2));
        mockContest.setIsOpen(false);
        mockContest.setContestEndingTime(LocalDateTime.now().minusHours(10));
        User user = createMockUser();
        user.setUserRank(createMockUserRank());
        user.setPoints(20);
        mockContest.setParticipants(Set.of(user));

        Mockito.when(contestRepository.getAll()).thenReturn(contestList);
        contestService.checkPhase();

        Mockito.verify(contestWinnerRepostitory, Mockito.times(1))
                .create(Mockito.any(ContestWinner.class));

    }

    @Test
    public void checkForUserEntry_should_returnTrue_whenUserHasEntered() {
        Contest contest= createMockContest();
        Mockito.when(contestRepository.getById(contest.getId()))
                .thenReturn(contest);
        contest.setParticipants(Set.of(createMockUser()));

        contestService.checkForUserEntry(contest.getId(), createMockUser());

        Assertions.assertTrue(contest.getParticipants().contains(createMockUser()));
    }

    @Test
    public void inviteJunkies_should_callContestInvitedJukieRepository() {
        int[] invitedJunkies = {1};

        contestService.inviteJunkies(invitedJunkies, 1);

        Mockito.verify(contestInvitedJunkieRepository, Mockito.times(1))
                .create(Mockito.any(ContestInvitedJunkie.class));
    }

    @Test
    public void getByJuror_should_returnContests() {
        List<ContestsJurors> contestsJurors = List.of(createMockContestsJurors());
        List<Contest> contests = List.of(createMockContest());
        Mockito.when(contestsJurorsRepository.getByUserId(1))
                .thenReturn(contestsJurors);

        Mockito.when(contestRepository.getById(1))
                .thenReturn(createMockContest());

        contestService.getByJuror(1);

        Assertions.assertEquals(contests.size(), 1);
    }

    @Test
    public void getAllContestImageEvaluations_should_returnEvaluationas() {
        List<ImageEvaluationDao> evaluations = new ArrayList<>();
        ImageEvaluationDao imageEvaluationDao = createMockImageEvaluationDao();
        ImageEvaluationDao imageEvaluationDao1 = createMockImageEvaluationDao();
        imageEvaluationDao1.setScore(8);
        imageEvaluationDao1.setRankingPlace(2);
        ImageEvaluationDao imageEvaluationDao2 = createMockImageEvaluationDao();
        imageEvaluationDao1.setScore(5);
        imageEvaluationDao1.setRankingPlace(3);
        Contest contest = createMockContest();
        Image image1 = createMockImage();
        image1.setTitle("titleTitle");
        Image image2 = createMockImage();
        image1.setTitle("titleTitle2");
        contest.setImages(Set.of(createMockImage(), image1, image2));

        Mockito.when(contestRepository.getById(contest.getId()))
                .thenReturn(contest);

        contestService.getAllContestImageEvaluations(1);
        Assertions.assertEquals(contest.getImages().size(), 3);

    }
}