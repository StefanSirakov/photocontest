package com.photocontest.photocontest.services;

import com.photocontest.exceptions.DuplicateEntityException;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.User;
import com.photocontest.models.UserRank;
import com.photocontest.repositories.contracts.UserRankRepository;
import com.photocontest.repositories.contracts.UserRepository;
import com.photocontest.services.UserServiceImpl;
import org.hamcrest.core.AnyOf;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static com.photocontest.photocontest.Helpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        userService.getAll();

        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_should_returnUser_when_matchExists() {
        User user = createMockUser();
        Mockito.when(mockRepository.getById(user.getId()))
                .thenReturn(user);
        User takenUser = userService.getById(user.getId());

        Assertions.assertEquals(user.getUsername(), takenUser.getUsername());

    }

    @Test
    public void getByUsername_should_return_User_when_matchExists(){
        User user = createMockUser();
        Mockito.when(mockRepository.getByField("username",user.getUsername()))
                .thenReturn(user);
        User takenUser = userService.getByUsername(user.getUsername());

        Assertions.assertEquals(user.getId(), takenUser.getId());
    }

    @Test
    public void getByEmail_should_return_User_when_matchExists(){
        User user = createMockUser();
        Mockito.when(mockRepository.getByField("email",user.getEmail()))
                .thenReturn(user);
        User takenUser = userService.getByEmail(user.getEmail());

        Assertions.assertEquals(user.getId(), takenUser.getId());
    }

    @Test
    public void create_should_throwException_when_usernameExists(){
        User user = createMockUser();
        Mockito.when(mockRepository.getByField("username", user.getUsername()))
                .thenReturn(user);

        Assertions.assertThrows(DuplicateEntityException.class, ()-> userService.create(user));
    }

    @Test
    public void create_should_throwException_when_emailExists(){
        User user = createMockUser();
        Mockito.when(mockRepository.getByField("username", user.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByField("email", user.getEmail()))
                .thenReturn(user);

        Assertions.assertThrows(DuplicateEntityException.class, ()-> userService.create(user));
    }

    @Test
    public void create_should_callRepository_when_validInfo(){
        User user = createMockUser();
        Mockito.when(mockRepository.getByField("username", user.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByField("email", user.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        userService.create(user);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(user);
    }

//    @Test
//    public void update_should_throwException_when_emailExists(){
//        User user = createMockUser();
//        User userToAuthenticate = createMockUser();
//        user.setEmail("asd");
//
//        Mockito.when(mockRepository.getByField("email", user.getEmail()))
//                .thenReturn(user);
//
//        Assertions.assertThrows(DuplicateEntityException.class, ()-> userService.update(user, userToAuthenticate));
//    }

    @Test
    public void update_should_callRepository_when_validInfo(){
        User user = createMockUser();
        User userToAuthenticate = createMockUser();

        Mockito.when(mockRepository.getByField("email", user.getEmail()))
                .thenReturn(user);

        userService.update(user, userToAuthenticate);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(user);
    }

    @Test
    public void delete_shouldThrow_UnauthorizedOperationException_when_notAuthorized(){
        User user = createMockUser();
        User user2 = createMockUser();
        user2.setUsername("asdasf");

        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> userService.delete(2, user));
    }

    @Test
    public void delete_should_callRepository_when_validInfo(){
        User user = createMockUser();

        Mockito.when(mockRepository.getById(1))
                .thenReturn(user);

        userService.delete(1, user);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(user);
    }

    @Test
    public void search_should_throwException_when_notAuthorized(){
        User user = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> userService.search(Optional.empty(), user));
    }

    @Test
    void getAllOrganizers_should_callRepository() {
        Mockito.when(mockRepository.getAllOrganizers())
                .thenReturn(new ArrayList<>());

        userService.getAllOrganizers();

        Mockito.verify(mockRepository, Mockito.times(1)).getAllOrganizers();
    }

    @Test
    void getAllOrderedByPoints_should_callRepository() {
        Mockito.when(mockRepository.getAllOrderedByPoints())
                .thenReturn(new ArrayList<>());

        userService.getAllOrderedByPoints();

        Mockito.verify(mockRepository, Mockito.times(1)).getAllOrderedByPoints();
    }

//    @Test
//    void getUserByPasswordResetToken_should_callRepository() {
//        Mockito.doNothing().when(mockRepository.getByField("password_reset_token", "asd"));
//
//        userService.getUserByPasswordResetToken("asd");
//
//        Mockito.verify(mockRepository, Mockito.times(1)).getByField("password_reset_token", "asd");
//    }

    @Test
    void changeUserPassword_should_callRepository() {
        User user = createMockUser();

        Mockito.when(mockRepository.getById(user.getId()))
                .thenReturn(user);

        userService.changeUserPassword(user,"asd");

        Mockito.verify(mockRepository, Mockito.times(1)).update(Mockito.any(User.class));
    }

    @Test
    void updateResetPasswordToken_should_callRepository() {
        User user = createMockUser();

        Mockito.when(mockRepository.getByField("email",user.getEmail()))
                .thenReturn(user);

        userService.updateResetPasswordToken("asd",user.getEmail());

        Mockito.verify(mockRepository, Mockito.times(1)).update(Mockito.any(User.class));
    }

    @Test
    void getAllJunkies_should_callRepository() {
        Mockito.when(mockRepository.getAllJunkies())
                .thenReturn(new ArrayList<>());

        userService.getAllJunkies();

        Mockito.verify(mockRepository, Mockito.times(1)).getAllJunkies();
    }

    @Test
    void promoteUserToOrganizer_should_throwException_when_notAuthorized() {
        User user = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                ()-> userService.promoteToOrganizer(user, user));
    }

    @Test
    void filter_should_callRepository() {
        Mockito.when(mockRepository.filter(Optional.empty(),Optional.empty(),Optional.empty()))
                .thenReturn(new ArrayList<>());

        userService.filter(Optional.empty(),Optional.empty(),Optional.empty());

        Mockito.verify(mockRepository, Mockito.times(1)).filter(
                Optional.empty(),Optional.empty(),Optional.empty());
    }

    @Test
    void getAllEligibleForJuror_should_callRepository() {
        Mockito.when(mockRepository.getAllEligibleForJuror(3,4))
                .thenReturn(new ArrayList<>());

        userService.getAllEligibleForJuror();

        Mockito.verify(mockRepository, Mockito.times(1)).getAllEligibleForJuror(3,4);
    }
}
