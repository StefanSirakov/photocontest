package com.photocontest.photocontest;

import com.photocontest.models.*;
import com.photocontest.models.daos.ImageEvaluationDao;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Helpers {

    public static Category createMockCategory() {
        var mockCategory = new Category();
        mockCategory.setId(1);
        mockCategory.setName("TestCategory");
        return mockCategory;
    }

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setLastName("MockLastName");
        mockUser.setPassword("MockPassword");
        mockUser.setFirstName("MockFirstName");
        mockUser.setOrganiser(false);
        return mockUser;
    }

    public static User createMockOrganizer() {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setLastName("MockLastName");
        mockUser.setPassword("MockPassword");
        mockUser.setFirstName("MockFirstName");
        mockUser.setOrganiser(true);
        return mockUser;
    }

    public static Review createMockReview() {
        var mockReview = new Review();
        mockReview.setId(1);
        mockReview.setScore(10);
        mockReview.setComment("comment comment");
        mockReview.setJuror(createMockOrganizer());
        return mockReview;
    }

    public static Image createMockImage() {
        var mockImage = new Image();
        mockImage.setId(1);
        mockImage.setImageAbsolutePath("absolutePath");
        mockImage.setUser(createMockUser());
        mockImage.setTitle("Title title");
        mockImage.setStory("Story story");
        mockImage.setContestId(1);
        mockImage.setTotalScore(10);
        mockImage.setReviews(new HashSet<>());
        return mockImage;
    }

    public static Contest createMockContest() {
        var mockContest = new Contest();
        mockContest.setId(1);
        mockContest.setTitle("Title Title");
        mockContest.setCategory(createMockCategory());
        mockContest.setCreator(createMockOrganizer());
        mockContest.setIsOpen(true);
        mockContest.setPhase(1);
        mockContest.setContest_starting_time(LocalDateTime.now());
        mockContest.setPhaseTwoStartingTime(LocalDateTime.now().plusHours(2));
        mockContest.setContestEndingTime(LocalDateTime.now().plusHours(3));
        mockContest.setBackgroundImageAbsPath("background image");
        mockContest.setImages(new HashSet<>());
        mockContest.setJurry(new HashSet<>());
        mockContest.setParticipants(new HashSet<>());
        mockContest.setInvitedJunkies(new HashSet<>());
        return mockContest;
    }

    public static ImagesReviews createMockImagesReviews() {
        var mockImagesReviews = new ImagesReviews();
        mockImagesReviews.setId(1);
        mockImagesReviews.setImageId(1);
        mockImagesReviews.setReviewId(1);
        return mockImagesReviews;
    }

    public static ContestParticipants createMockContestParticipants() {
        var mockContestParticipants = new ContestParticipants();
        mockContestParticipants.setId(1);
        mockContestParticipants.setContestId(1);
        mockContestParticipants.setUserId(1);
        return mockContestParticipants;
    }

    public static ContestCategory createMockContestCategory() {
        var mockContestCategory = new ContestCategory();
        mockContestCategory.setId(1);
        mockContestCategory.setContestId(1);
        mockContestCategory.setCategoryId(1);
        return mockContestCategory;
    }

    public static UserRank createMockUserRank() {
        var mockUserRank = new UserRank();
        mockUserRank.setId(1);
        mockUserRank.setName("Junkie");
        return mockUserRank;
    }

    public static ContestWinner createMockContestWinner() {
        var mockContestWinner = new ContestWinner();
        mockContestWinner.setId(1);
        mockContestWinner.setContest(createMockContest());
        mockContestWinner.setImage(createMockImage());
        mockContestWinner.setPlace(1);
        return mockContestWinner;
    }

    public static ContestInvitedJunkie createMockContestInvitedJunkie() {
        var mockContestInvitedJunkie = new ContestInvitedJunkie();
        mockContestInvitedJunkie.setId(1);
        mockContestInvitedJunkie.setContestId(1);
        mockContestInvitedJunkie.setUserId(1);
        return mockContestInvitedJunkie;
    }

    public static ContestsJurors createMockContestsJurors() {
        var mockContestsJurors = new ContestsJurors();
        mockContestsJurors.setId(1);
        mockContestsJurors.setContest(createMockContest());
        mockContestsJurors.setJuror(createMockOrganizer());
        return mockContestsJurors;
    }

    public static ImageEvaluationDao createMockImageEvaluationDao() {
        var mockImageEvaluationDao = new ImageEvaluationDao();
        mockImageEvaluationDao.setImage(createMockImage());
        mockImageEvaluationDao.setReviews(new HashSet<>());
        mockImageEvaluationDao.setRankingPlace(1);
        mockImageEvaluationDao.setScore(10);
        return mockImageEvaluationDao;
    }
}
