package com.photocontest.services;

import com.photocontest.exceptions.DuplicateEntityException;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.DTOs.PasswordUpdateDTO;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.UserRankRepository;
import com.photocontest.repositories.contracts.UserRepository;
import com.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    public static final int MASTER = 3;
    public static final int WISE_AND_BENEVOLENT = 4;
    private final UserRepository userRepository;
    private final UserRankRepository rankRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserRankRepository rankRepository) {
        this.userRepository = userRepository;
        this.rankRepository = rankRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByField("username", username);
    }

    @Override
    public User getByEmail(String userEmail) {
        return userRepository.getByField("email", userEmail);
    }

    @Override
    public void create(User user) {
        boolean duplicateExists = true;
        try {
            userRepository.getByField("username", user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }
        duplicateExists = true;

        try {
            userRepository.getByField("email", user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        userRepository.create(user);
    }

    @Override
    public void update(User userToAuthenticate, User user) {
        boolean duplicateExists = true;
        try {
            userRepository.getByField("email", user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists && (user.getId() != (userToAuthenticate.getId()))) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        if (!(user.getId() == userToAuthenticate.getId())) {
            throw new UnauthorizedOperationException("Unauthorized Operation.");
        }
        userRepository.update(user);
    }

    @Override
    public void delete(int id, User user) {
        if (!(id == user.getId())) {
            throw new UnauthorizedOperationException("Unauthorized Operation.");
        }
        user = userRepository.getById(id);
        user.setDeleted(true);
        userRepository.update(user);
    }

    @Override
    public List<User> search(Optional<String> search, User user) {
        return userRepository.search(search);
    }

    @Override
    public Set<User> getAllOrganizers() {
        return new HashSet<>(userRepository.getAllOrganizers());
    }

    @Override
    public List<User> getAllOrderedByPoints() {
        return userRepository.getAllOrderedByPoints();
    }

    @Override
    public User getUserByPasswordResetToken(String token) {
        return userRepository.getByField("reset_password_token", token);
    }

    @Override
    public void changeUserPassword(User user, String newPassword) {
        User userById = userRepository.getById(user.getId());
        userById.setPassword(newPassword);
        userRepository.update(userById);
    }

    @Override
    public void updateResetPasswordToken(String token, String email) {
            User user = userRepository.getByField("email", email);
            user.setResetPasswordToken(token);
            userRepository.update(user);
    }

    @Override
    public Set<User> getAllJunkies() {
        return new HashSet<>(userRepository.getAllJunkies());
    }

    @Override
    public void promoteToOrganizer(User userToBePromoted, User userFromSession) {
        if(userFromSession.isOrganiser()) {
            userToBePromoted.setOrganiser(true);
            userToBePromoted.setPoints(-1);
            userRepository.update(userToBePromoted);
        } else {
            throw new UnauthorizedOperationException("Only Organizers can promote.");
        }
    }

    @Override
    public List<User> filter(Optional<String> search, Optional<Integer> minPoints, Optional<Boolean> role) {
        return userRepository.filter(search, minPoints, role);
    }

    @Override
    public void updatePassword(User user, int id, PasswordUpdateDTO dto) {
        user = userRepository.getById(user.getId());

        if(user.getId() != id) {
            throw new UnauthorizedOperationException("You are not allowed to perform that action.");
        }
        if(!dto.getOldPassword().equals(user.getPassword())) {
            throw new IllegalArgumentException("Your old password is incorrect");
        }
        if(!dto.getNewPassword().equals(dto.getNewPasswordConfirm())) {
            throw new IllegalArgumentException("The new passwords do not match.");
        }
        user.setPassword(dto.getNewPassword());
        userRepository.update(user);
    }

    @Override
    public List<User> getAllEligibleForJuror() {
        return userRepository.getAllEligibleForJuror(MASTER, WISE_AND_BENEVOLENT);
    }



}
