package com.photocontest.services;

import com.photocontest.exceptions.DuplicateEntityException;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.mappers.ImageEvaluationDaoMapper;
import com.photocontest.models.*;
import com.photocontest.models.daos.ImageEvaluationDao;
import com.photocontest.models.enums.ContestPhase;
import com.photocontest.repositories.contracts.*;
import com.photocontest.services.contracts.ContestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ContestServiceImpl implements ContestService {
    public static final String UNAUTHORIZED_OPERATION = "Unauthorized operation.";
    public static final int FIRST_PLACE_FULL_POINTS = 50;
    public static final int SECOND_PLACE_FULL_POINTS = 35;
    public static final int THIRD_PLACE_FULL_POINTS = 20;
    public static final int THIRD_PLACE_SHARED_POINTS = 10;
    public static final int SECOND_PLACE_SHARED_POINTS = 25;
    public static final int FIRST_PLACE_SHARED_POINTS = 40;
    public static final int FIRST_PLACE_POINTS_WITH_DOUBLE_SCORE = 75;
    public static final int POINTS_FOR_PARTICIPATION_IN_OPEN_CONTEST = 1;
    public static final int POINTS_FOR_PARTICIPATION_IN_INVITATIONAL_CONTEST = 3;
    public static int PHASE_ONE = 1;
    public static int PHASE_TWO = 2;
    public static int END_CONTEST = 3;

    public static int FIRST_PLACE = 1;
    public static int SECOND_PLACE = 2;
    public static int THIRD_PLACE = 3;

    private final ContestRepository contestRepository;
    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;
    private final ImageRepository imageRepository;
    private final ContestInvitedJunkieRepository contestInvitedJunkieRepository;
    private final ContestCategoryRepository contestCategoryRepository;
    private final ContestParticipantsRepository contestParticipantsRepository;
    private final UserRankRepository userRankRepository;
    private final ContestsJurorsRepository contestsJurorsRepository;
    private final ContestWinnerRepostitory contestWinnerRepostitory;
    private final ImageEvaluationDaoMapper imageEvaluationDaoMapper;

    @Autowired
    public ContestServiceImpl(ContestRepository contestRepository, UserRepository userRepository,
                              CategoryRepository categoryRepository, ImageRepository imageRepository,
                              ContestInvitedJunkieRepository contestInvitedJunkieRepository,
                              ContestCategoryRepository contestCategoryRepository, ContestParticipantsRepository contestParticipantsRepository,
                              UserRankRepository userRankRepository, ContestsJurorsRepository contestsJurorsRepository,
                              ContestWinnerRepostitory contestWinnerRepostitory, ImageEvaluationDaoMapper imageEvaluationDaoMapper) {
        this.contestRepository = contestRepository;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.imageRepository = imageRepository;
        this.contestInvitedJunkieRepository = contestInvitedJunkieRepository;
        this.contestCategoryRepository = contestCategoryRepository;
        this.contestParticipantsRepository = contestParticipantsRepository;
        this.userRankRepository = userRankRepository;
        this.contestsJurorsRepository = contestsJurorsRepository;
        this.contestWinnerRepostitory = contestWinnerRepostitory;
        this.imageEvaluationDaoMapper = imageEvaluationDaoMapper;
    }


    @Override
    public List<Contest> getAll() {
        List<Contest> contests = contestRepository.getAll();
        contests.sort(Comparator.comparing(Contest::getId).reversed());
        return contests;
    }

    @Override
    public Contest getById(int id) {
        return contestRepository.getById(id);
    }

    @Override
    public List<Contest> getByUserEntry(int userId) {
        List<ContestParticipants> contestParticipants = contestParticipantsRepository.getByUserEntry(userId);
        List<Contest> contests = new ArrayList<>();
        for (ContestParticipants contestParticipant : contestParticipants) {
            contests.add(contestRepository.getById(contestParticipant.getContestId()));
        }
        return contests;
    }


    @Override
    public Contest getByUsername(String username) {
        return contestRepository.getByField("username", username);
    }

    @Override
    public void create(Contest contest, String categoryName, User creator) {
        boolean duplicateExists = true;
        if (!creator.isOrganiser()) {
            throw new UnauthorizedOperationException("Only organizers are allowed to do that.");
        }

        try {
            contestRepository.getByField("title", contest.getTitle());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Contest", "title", contest.getTitle());
        }
        Category category = null;
        try {
            category = categoryRepository.getByField("name", categoryName);
        } catch (EntityNotFoundException e) {
            category = new Category(categoryName);
            categoryRepository.create(category);
        }

        contest.setCategory(category);
        contest.setCreator(creator);

        contestRepository.create(contest);

        ContestCategory contestCategory = new ContestCategory(contest.getId(), category.getId());
        contestCategoryRepository.create(contestCategory);
    }

    @Override
    public void update(Contest contest, User userToAuthenticate) {
        boolean duplicateExists = true;
        if (!(userToAuthenticate.isOrganiser())) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_OPERATION);
        }
        try {
            Contest contestToUpdate = contestRepository.getByField("title", contest.getTitle());
            if (contestToUpdate.getId() == contest.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Contest", "title", contest.getTitle());
        }
        contestRepository.update(contest);
    }

    @Override
    public void delete(int id, User user) {
        if ((user.isOrganiser())) {
            contestRepository.delete(id);
        } else {
            throw new UnauthorizedOperationException(UNAUTHORIZED_OPERATION);
        }
    }

    @Override
    public List<Contest> search(Optional<String> search) {
        return contestRepository.search(search);
    }

    @Override
    public List<Contest> filter(Optional<String> title, Optional<Integer> category, Optional<String> phase) {
        Optional<Integer> phaseId = Optional.empty();
        ;
        if (phase.isPresent()) {
            if (phase.equals(Optional.of("Phase I"))) phaseId = Optional.of(1);
            if (phase.equals(Optional.of("Phase II"))) phaseId = Optional.of(2);
            if (phase.equals(Optional.of("Finished"))) phaseId = Optional.of(3);
        }
        return contestRepository.filter(title, category, phaseId);
    }

    @Override
    public void checkPhase() {
        for (Contest contestToCheck : contestRepository.getAll()) {
            if (contestToCheck.getPhase() != ContestPhase.FINISHED) {
                if (contestToCheck.getPhaseTwoStartingTime().isBefore(LocalDateTime.now())) {
                    contestToCheck.setPhase(PHASE_TWO);
                    contestRepository.update(contestToCheck);
                }
            }
            if (contestToCheck.getContestEndingTime().isBefore(LocalDateTime.now())) {
                if (contestToCheck.isInPhaseTwo()) {
                    distributePointsToParticipants(contestToCheck);
                    updateUserRanks(contestToCheck);
                }
                contestToCheck.setPhase(END_CONTEST);
                contestRepository.update(contestToCheck);
            }

        }
    }

    private void updateUserRanks(Contest contestToCheck) {
        for (User participant : contestToCheck.getParticipants()) {
            if (participant.pointsToNextRank() < 0) {
                participant.setUserRank(userRankRepository.getById(participant.getUserRank().getId() + 1));
                userRepository.update(participant);
            }
        }
    }

    private void distributePointsToParticipants(Contest contest) {

        if (contest.getImages().size() == 0) {
            return;
        }

        addPointsForParticipating(contest);
        Set<Image> images = addDefaultPointsFromJuryThatHasNotVoted(contest);

        List<Image> entriesSortedByPoints = images
                .stream()
                .sorted(Comparator.comparing(Image::getTotalScore).reversed())
                .collect(Collectors.toList());

        if (entriesSortedByPoints.size() == 1) {
            entriesSortedByPoints.get(0).getUser().addPoints(FIRST_PLACE_FULL_POINTS);
            userRepository.update(entriesSortedByPoints.get(0).getUser());

            contestWinnerRepostitory.create(new ContestWinner(contest, entriesSortedByPoints.get(0), FIRST_PLACE));
            return;
        }

        int index;
        if (entriesSortedByPoints.get(0).getTotalScore() > entriesSortedByPoints.get(1).getTotalScore()) {
            if (entriesSortedByPoints.get(0).getTotalScore() >= entriesSortedByPoints.get(1).getTotalScore() * 2) {
                entriesSortedByPoints.get(0).getUser().addPoints(FIRST_PLACE_POINTS_WITH_DOUBLE_SCORE);
                userRepository.update(entriesSortedByPoints.get(0).getUser());

                contestWinnerRepostitory.create(new ContestWinner(contest, entriesSortedByPoints.get(0), FIRST_PLACE));
            } else {
                entriesSortedByPoints.get(0).getUser().addPoints(FIRST_PLACE_FULL_POINTS);
                userRepository.update(entriesSortedByPoints.get(0).getUser());

                contestWinnerRepostitory.create(new ContestWinner(contest, entriesSortedByPoints.get(0), FIRST_PLACE));
            }
            index = distributeSecondAndThirdPlacePoints(contest,
                    entriesSortedByPoints, 1, SECOND_PLACE_FULL_POINTS, SECOND_PLACE_SHARED_POINTS);
            distributeSecondAndThirdPlacePoints(contest,
                    entriesSortedByPoints, index, THIRD_PLACE_FULL_POINTS, THIRD_PLACE_SHARED_POINTS);
        } else {
            for (int i = 0; i < entriesSortedByPoints.size() - 2; i++) {
                if (entriesSortedByPoints.get(i).getTotalScore() == entriesSortedByPoints.get(i + 1).getTotalScore()) {
                    entriesSortedByPoints.get(i).getUser().addPoints(FIRST_PLACE_SHARED_POINTS);
                    userRepository.update(entriesSortedByPoints.get(i).getUser());

                    contestWinnerRepostitory.create(new ContestWinner(contest, entriesSortedByPoints.get(i), FIRST_PLACE));
                } else {
                    entriesSortedByPoints.get(i).getUser().addPoints(FIRST_PLACE_SHARED_POINTS);
                    userRepository.update(entriesSortedByPoints.get(i).getUser());
                    index = distributeSecondAndThirdPlacePoints(contest,
                            entriesSortedByPoints, i + 1, SECOND_PLACE_FULL_POINTS, SECOND_PLACE_SHARED_POINTS);
                    distributeSecondAndThirdPlacePoints(contest,
                            entriesSortedByPoints, index, THIRD_PLACE_FULL_POINTS, THIRD_PLACE_SHARED_POINTS);
                }
            }
        }
    }

    private int distributeSecondAndThirdPlacePoints(Contest contest, List<Image> sortedImages, int startingIndex,
                                                    int fullPoints, int sharedPoints) {
        if (sortedImages.size() == startingIndex + 1) {
            sortedImages.get(startingIndex).getUser().addPoints(fullPoints);
            userRepository.update(sortedImages.get(startingIndex).getUser());
            if (fullPoints == SECOND_PLACE_FULL_POINTS && sharedPoints == SECOND_PLACE_SHARED_POINTS) {
                contestWinnerRepostitory.create(new ContestWinner(contest, sortedImages.get(startingIndex), SECOND_PLACE));
            } else {
                contestWinnerRepostitory.create(new ContestWinner(contest, sortedImages.get(startingIndex), THIRD_PLACE));
            }
            return startingIndex + 1;
        }
        if (sortedImages.size() > startingIndex) {
            if (sortedImages.get(startingIndex).getTotalScore() > sortedImages.get(startingIndex + 1).getTotalScore()) {
                sortedImages.get(startingIndex).getUser().addPoints(fullPoints);
                userRepository.update(sortedImages.get(startingIndex).getUser());
                if (fullPoints == SECOND_PLACE_FULL_POINTS && sharedPoints == SECOND_PLACE_SHARED_POINTS) {
                    contestWinnerRepostitory.create(new ContestWinner(contest, sortedImages.get(startingIndex), SECOND_PLACE));
                } else {
                    contestWinnerRepostitory.create(new ContestWinner(contest, sortedImages.get(startingIndex), THIRD_PLACE));
                }
            } else {
                for (int i = startingIndex; i < sortedImages.size() - 2; i++) {
                    sortedImages.get(startingIndex).getUser().addPoints(sharedPoints);
                    if (sortedImages.get(i).getTotalScore() > sortedImages.get(i + 1).getTotalScore()) {
                        sortedImages.get(i).getUser().addPoints(sharedPoints);
                        userRepository.update(sortedImages.get(i).getUser());
                        startingIndex += i;
                    }
                    if (fullPoints == SECOND_PLACE_FULL_POINTS && sharedPoints == SECOND_PLACE_SHARED_POINTS) {
                        contestWinnerRepostitory.create(new ContestWinner(contest, sortedImages.get(startingIndex), SECOND_PLACE));
                    } else {
                        contestWinnerRepostitory.create(new ContestWinner(contest, sortedImages.get(startingIndex), THIRD_PLACE));
                    }
                }
            }
        }
        return startingIndex + 1;
    }

    private void addPointsForParticipating(Contest contest) {
        if (contest.getIsOpen()) {
            for (User participant : contest.getParticipants()) {
                participant.addPoints(POINTS_FOR_PARTICIPATION_IN_OPEN_CONTEST);
                userRepository.update(participant);
            }
        } else {
            for (User participant : contest.getParticipants()) {
                participant.addPoints(POINTS_FOR_PARTICIPATION_IN_INVITATIONAL_CONTEST);
                userRepository.update(participant);
            }
        }

    }

    private Set<Image> addDefaultPointsFromJuryThatHasNotVoted(Contest contest) {
        for (Image image : contest.getImages()) {
            image.addPoints((contest.getJurry().size() - image.getReviews().size()) * 3);
            imageRepository.update(image);
        }
        return contest.getImages();
    }

    @Override
    public boolean checkForUserEntry(int contestId, User user) {
        Contest contest = contestRepository.getById(contestId);
        return contest.getParticipants().contains(user);
    }

    @Override
    public void inviteJunkies(int[] invitedJunkies, int contestId) {
        for (int invitedJunkieId : invitedJunkies) {
            ContestInvitedJunkie contestInvitedJunkie = new ContestInvitedJunkie(contestId, invitedJunkieId);
            contestInvitedJunkieRepository.create(contestInvitedJunkie);
        }
    }

    @Override
    public List<Contest> getByJuror(int userId) {
        List<ContestsJurors> contestsJurors = contestsJurorsRepository.getByUserId(userId);
        List<Contest> contests = new ArrayList<>();
        for (ContestsJurors contestsJuror : contestsJurors) {
            contests.add(contestRepository.getById(contestsJuror.getContest().getId()));
        }
        return contests;
    }

    @Override
    public List<ImageEvaluationDao> getAllContestImageEvaluations(int contestId) {
        List<ImageEvaluationDao> evaluations = new ArrayList<>();

        Contest contest = contestRepository.getById(contestId);
        var images = contest.getImages();

        images.forEach(image -> {
            var imageEvaluationDto = new ImageEvaluationDao();
            imageEvaluationDto.setImage(image);
            imageEvaluationDto.setScore(image.getReviews()
                    .stream()

                    .mapToInt(Review::getScore).sum());
            imageEvaluationDto.setReviews(image.getReviews());

            evaluations.add(imageEvaluationDto);
        });
        evaluations.sort((a, b) -> Integer.compare(b.getScore(), a.getScore()));
        if (!evaluations.isEmpty()) {
            int place = FIRST_PLACE;
            evaluations.get(0).setRankingPlace(place);
            for (int i = 1; i < evaluations.size(); i++) {

                int prevScore = evaluations.get(i - 1).getScore();
                int currScore = evaluations.get(i).getScore();
                if (prevScore == currScore) {
                    evaluations.get(i).setRankingPlace(place);
                } else {
                    evaluations.get(i).setRankingPlace(++place);
                }
            }
            evaluations.removeIf(evaluation -> evaluation.getRankingPlace() != 1 && evaluation.getRankingPlace() != 2 && evaluation.getRankingPlace() != 3);
        }
        return evaluations;
    }

    @Override
    public List<ImageEvaluationDao> getTopPhotos() {

        var evaluations = new ArrayList<ImageEvaluationDao>();

        contestWinnerRepostitory.getTopPhotos()
                .forEach(winner -> evaluations.add(
                        imageEvaluationDaoMapper.compileImageEvaluation(
                                winner.getImage(),
                                winner.getImage().getReviews(),
                                winner.getImage().getTotalScore(),
                                winner.getPlace())));
        return evaluations;
    }
}
