package com.photocontest.services;

import com.photocontest.models.Category;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.CategoryRepository;
import com.photocontest.services.contracts.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public List<Category> getAll() {
        return categoryRepository.getAll();
    }

    @Override
    public Category getById(int id) {
        return categoryRepository.getById(id);
    }

    @Override
    public Category getByName(String name) {
        return categoryRepository.getByField("name", name);
    }


    @Override
    public void delete(int id, User user) {
        Category categoryToDelete = categoryRepository.getById(id);
        categoryRepository.delete(id);
    }

    @Override
    public List<Category> search(Optional<String> search)    {
        return categoryRepository.search(search);
    }
}
