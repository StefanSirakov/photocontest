package com.photocontest.services;

import com.photocontest.exceptions.DuplicateEntityException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.exceptions.UserAlreadyParticipatingException;
import com.photocontest.models.Contest;
import com.photocontest.models.ContestParticipants;
import com.photocontest.models.Image;
import com.photocontest.models.User;
import com.photocontest.repositories.contracts.ContestParticipantsRepository;
import com.photocontest.repositories.contracts.ContestRepository;
import com.photocontest.repositories.contracts.ImageRepository;
import com.photocontest.services.contracts.ContestService;
import com.photocontest.services.contracts.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ImageServiceImpl implements ImageService {
    public static final String ONLY_ONE_SUBMISSION_PER_USER_IS_ALLOWED = "Only one submission per user is allowed!";
    private final ImageRepository imageRepository;
    private final ContestService contestService;
    private final ContestRepository contestRepository;
    private final ContestParticipantsRepository contestParticipantsRepository;


    @Autowired
    public ImageServiceImpl(ImageRepository imageRepository, ContestService contestService, ContestRepository contestRepository, ContestParticipantsRepository contestParticipantsRepository) {
        this.imageRepository = imageRepository;
        this.contestService = contestService;
        this.contestRepository = contestRepository;
        this.contestParticipantsRepository = contestParticipantsRepository;
    }

//    @Override
    public List<Image> getAll() {
        return imageRepository.getAll();
    }

    @Override
    public List<Image> sortByScore() {
       return imageRepository.getAll().stream()
                .sorted(Comparator.comparing(Image::getTotalScore).reversed())
                .collect(Collectors.toList());
    }
    @Override
    public Image getById(int imageId) {
        return imageRepository.getById(imageId);
    }

    @Override
    public List<Image> getByUserId(int id) {
        return imageRepository.getByUserId(id);
    }

    @Override
    public void create(Image image, int contestId) {
        Contest contest = contestService.getById(contestId);

        if (contest.getParticipants().contains(image.getUser())){
            throw new UserAlreadyParticipatingException(ONLY_ONE_SUBMISSION_PER_USER_IS_ALLOWED);
        }

        if (contestService.checkForUserEntry(contestId, image.getUser())) {
            throw new DuplicateEntityException("You already uploaded an image for this contest.");
        } else {
            if(!contest.getIsOpen() && !(contest.getInvitedJunkies().contains(image.getUser()))) {
                throw new UnauthorizedOperationException("You were not invited to participate in this contest.");
            }
            ContestParticipants contestParticipants = new ContestParticipants(contestId, image.getUser().getId());
            contestParticipantsRepository.create(contestParticipants);

            image.setContestId(contestId);
            imageRepository.create(image);
        }
    }

    @Override
    public void delete(int contestId, int imageId, User user) {
        Contest contest = contestService.getById(contestId);
        if(!(contest.getJurry().contains(user))) {
            throw new UnauthorizedOperationException("Only the jury is allowed to delete photos from contests.");
        }
        imageRepository.delete(imageId);
    }

    @Override
    public List<Image> searchPhoto(Optional<String> search, User user, int contestId) {
        if(user.isOrganiser()){
            return imageRepository.search(search, contestId);
        }
        throw new UnauthorizedOperationException("Unauthorized operation.");
    }
}
