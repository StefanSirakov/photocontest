package com.photocontest.services.contracts;

import com.photocontest.models.DTOs.PasswordUpdateDTO;
import com.photocontest.models.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserService {

    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    User getByEmail(String userEmail);

    void create(User user);

    void update(User userToAuthenticate, User user);

    void delete(int id, User user);

    List<User> search(Optional<String> search, User user);

    Set<User> getAllOrganizers();

    List<User> getAllOrderedByPoints();

    User getUserByPasswordResetToken(String token);

    void changeUserPassword(User user, String newPassword);

    void updateResetPasswordToken(String token, String email);


    List<User> getAllEligibleForJuror();

    Set<User> getAllJunkies();

    void promoteToOrganizer(User userToBePromoted, User userFromSession);

    List<User> filter(Optional<String> search, Optional<Integer> minPoints, Optional<Boolean> role);

    void updatePassword(User userFromSession, int id, PasswordUpdateDTO dto);
}
