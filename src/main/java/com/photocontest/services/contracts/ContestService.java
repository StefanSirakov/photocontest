package com.photocontest.services.contracts;

import com.photocontest.models.Contest;
import com.photocontest.models.User;
import com.photocontest.models.daos.ImageEvaluationDao;

import java.util.List;
import java.util.Optional;

public interface ContestService {

    List<Contest> getAll();

    Contest getById(int id);

    List<Contest> getByUserEntry(int userId);

    Contest getByUsername(String username);

    void create(Contest contest, String categoryName, User creator);

    void update(Contest contest, User userToAuthenticate);

    void delete(int id, User user);

    List<Contest> search(Optional<String> search);

    List<Contest> filter(Optional<String> title, Optional<Integer> category, Optional<String> phase);

    void checkPhase();

    boolean checkForUserEntry(int contestId, User user);

    void inviteJunkies(int[] invitedJunkies, int contestId);

    List<Contest> getByJuror(int userId);

    List<ImageEvaluationDao> getAllContestImageEvaluations(int contestId);

    List<ImageEvaluationDao> getTopPhotos();
}
