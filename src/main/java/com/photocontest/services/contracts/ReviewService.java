package com.photocontest.services.contracts;

import com.photocontest.models.Review;
import com.photocontest.models.User;

import java.util.List;

public interface ReviewService {

    List<Review> getAll();

    Review getById(int id);

    void reviewImage(Review review, int imageId, User juror, int contestId);

//    void delete(int id, User user);
}
