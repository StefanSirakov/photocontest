package com.photocontest.services.contracts;

import com.photocontest.models.Category;
import com.photocontest.models.User;

import java.util.List;
import java.util.Optional;

public interface CategoryService {

    List<Category> getAll();

    Category getById(int id);

    Category getByName(String name);

    void delete(int id, User user);

    List<Category> search(Optional<String> search);
}
