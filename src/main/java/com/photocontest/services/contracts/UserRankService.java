package com.photocontest.services.contracts;

import com.photocontest.models.UserRank;

import java.util.List;

public interface UserRankService {

    List<UserRank> getAll();

    UserRank getById(int id);

}
