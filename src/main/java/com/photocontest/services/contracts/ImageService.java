package com.photocontest.services.contracts;

import com.photocontest.models.Image;
import com.photocontest.models.User;

import java.util.List;
import java.util.Optional;

public interface ImageService {
    void create(Image image, int contestId);

    List<Image> getAll();

    List<Image> sortByScore();

    void delete(int contestId, int imageId, User user);

    List<Image> searchPhoto(Optional<String> search, User user, int contestId);

    Image getById(int imageId);

    List<Image> getByUserId(int id);
}
