package com.photocontest.services;

import com.photocontest.models.UserRank;
import com.photocontest.repositories.contracts.UserRankRepository;
import com.photocontest.services.contracts.UserRankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRankServiceImpl implements UserRankService {

    private final UserRankRepository rankRepository;

    @Autowired
    public UserRankServiceImpl(UserRankRepository rankRepository) {
        this.rankRepository = rankRepository;
    }


    @Override
    public List<UserRank> getAll() {
        return rankRepository.getAll();
    }

    @Override
    public UserRank getById(int id) {
        return rankRepository.getById(id);
    }
}
