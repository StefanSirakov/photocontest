package com.photocontest.services;

import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.*;
import com.photocontest.repositories.contracts.ContestRepository;
import com.photocontest.repositories.contracts.ImageRepository;
import com.photocontest.repositories.contracts.ImagesReviewsRepository;
import com.photocontest.repositories.contracts.ReviewRepository;
import com.photocontest.services.contracts.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewServiceImpl implements ReviewService {

    public static final String ONLY_JUROR_CAN_RATE = "Unauthorized Operation! Only juror can rate a photo.";
    public static final String ALREADY_REVIEWED = "You already reviewed that image!";
    private final ReviewRepository reviewRepository;
    private final ImagesReviewsRepository imagesReviewsRepository;
    private final ImageRepository imageRepository;
    private final ContestRepository contestRepository;

    @Autowired
    public ReviewServiceImpl(ReviewRepository reviewRepository, ImagesReviewsRepository imagesReviewsRepository,
                             ImageRepository imageRepository, ContestRepository contestRepository) {
        this.reviewRepository = reviewRepository;
        this.imagesReviewsRepository = imagesReviewsRepository;
        this.imageRepository = imageRepository;
        this.contestRepository = contestRepository;
    }

    @Override
    public List<Review> getAll() {
        return reviewRepository.getAll();
    }

    @Override
    public Review getById(int id) {
        return reviewRepository.getById(id);
    }

    @Override
    public void reviewImage(Review review, int imageId, User juror, int contestId) {
        Contest contest = contestRepository.getById(contestId);
        Image image = imageRepository.getById(imageId);
        if (contest.getJurry().contains(juror)){
            if (!image.isEvaluatedBy(juror.getId())) {
                review.setJuror(juror);
                reviewRepository.create(review);
                ImagesReviews imagesReviews = new ImagesReviews(imageId, review.getId());
                imagesReviewsRepository.create(imagesReviews);
                image.addPoints(review.getScore());
                imageRepository.update(image);
            } else {
                throw new UnauthorizedOperationException(ALREADY_REVIEWED);
            }
        }else {
            throw new UnauthorizedOperationException(ONLY_JUROR_CAN_RATE);
        }
    }
}
