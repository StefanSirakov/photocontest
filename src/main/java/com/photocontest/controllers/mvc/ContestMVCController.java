package com.photocontest.controllers.mvc;

import com.photocontest.controllers.helpers.AuthenticationHelper;
import com.photocontest.exceptions.*;
import com.photocontest.mappers.ContestMapper;
import com.photocontest.mappers.ImageMapper;
import com.photocontest.models.*;
import com.photocontest.models.DTOs.*;
import com.photocontest.models.enums.ContestPhase;
import com.photocontest.services.contracts.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping("/contests")
public class ContestMVCController {

    public static final String NOT_RELEVANT = "The image is not in the right category!";
    private static final String UPLOADED_FOLDER = "C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\";
    private static final String CONTEST_BACKGROUND_IMAGE_FOLDER = "C:\\photo-contest\\src\\main\\resources\\static\\assets\\contestbackgrounds\\";

    private final ContestService contestService;
    private final ImageService imageService;
    private final AuthenticationHelper authenticationHelper;
    private final ImageMapper imageMapper;
    private final UserService userService;
    private final ContestMapper contestMapper;
    private final CategoryService categoryService;
    private final ReviewService reviewService;

    public ContestMVCController(ContestService contestService, ImageService imageService, AuthenticationHelper authenticationHelper,
                                ImageMapper imageMapper, UserService userService, ContestMapper contestMapper,
                                CategoryService categoryService, ReviewService reviewService) {
        this.contestService = contestService;
        this.imageService = imageService;
        this.authenticationHelper = authenticationHelper;
        this.imageMapper = imageMapper;
        this.userService = userService;
        this.contestMapper = contestMapper;
        this.categoryService = categoryService;
        this.reviewService = reviewService;
    }

    @ModelAttribute("isAuthorized")
    public boolean populateIsAuthorized(HttpSession session) {
        if (!(getUserId(session).equals(-1))) {
            return session.getAttribute("isOrganizer").equals(true);
        }
        return false;
    }

    @ModelAttribute("userID")
    public Object getUserId(HttpSession session) {
        if (session.getAttribute("userId") != null) {
            return session.getAttribute("userId");
        }
        return -1;
    }

    @ModelAttribute("eligibleJurors")
    public List<User> eligibleForJuryUsers() {
        return userService.getAllEligibleForJuror();
    }

    @ModelAttribute("junkies")
    public Set<User> getAllJunkies() {
        return userService.getAllJunkies();
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("phases")
    public ContestPhase[] getAllPhases() {
        return ContestPhase.values();
    }

    @GetMapping
    public String showAllContests(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }


        List<Contest> contests = contestService.getAll();
        model.addAttribute("contests", contests);
        model.addAttribute("filterContestDto", new FilterContestDTO());
        return "contests";
    }

    @PostMapping()
    public String filterContests(@ModelAttribute("filterContestDto") FilterContestDTO filterContestDto,
                                 Model model) {
        var filtered = contestService.filter(
                Optional.ofNullable(filterContestDto.getTitle().isBlank() ? null : filterContestDto.getTitle()),
                Optional.ofNullable(filterContestDto.getCategoryId() == null ? null : filterContestDto.getCategoryId()),
                Optional.ofNullable(filterContestDto.getPhase().isBlank() ? null : filterContestDto.getPhase())
        );
        model.addAttribute("contests", filtered);
        return "contests";
    }

    @GetMapping("/create")
    public String showContestCreateForm(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (!user.isOrganiser()) {
            return "access-denied";
        }

        model.addAttribute("user", user);
        model.addAttribute("newContest", new ContestDTO());
        return "contest-new";
    }

    @PostMapping("/create")
    public String createContest(Model model, HttpSession session,
                                @RequestParam(name = "file", required = false) MultipartFile file,
                                @Valid @ModelAttribute("newContest") ContestDTO contestDTO,
                                BindingResult errors, RedirectAttributes attributes) throws IOException {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            model.addAttribute("user", user);
            model.addAttribute("newContest", contestDTO);
            return "contest-new";
        }

        if (file.isEmpty()) {
            attributes.addFlashAttribute("message", "Please select a file to upload.");
            return "contest-new";
        }

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        Path path;

        try {
            path = Paths.get(CONTEST_BACKGROUND_IMAGE_FOLDER + fileName);
            Contest contest = contestMapper.fromDto(contestDTO, path);
            contest.setBackgroundImageAbsPath(path.toString());
            contestService.create(contest, contestDTO.getCategory(), user);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            model.addAttribute("user", user);
            model.addAttribute("newContest", contestDTO);
            return "contest-new";
        }
        Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        return "redirect:/contests";
    }


    @GetMapping("/{id}")
    public String showSingleContest(@PathVariable int id, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Contest contest = contestService.getById(id);
            model.addAttribute("contest", contest);
            model.addAttribute("newContest", new ContestDTO());
            model.addAttribute("images", contest.getImages());
            if (contest.getPhase().equals(ContestPhase.FINISHED)) {
                model.addAttribute("evaluations",
                        contestService.getAllContestImageEvaluations(contest.getId()));
            }
            return "contest";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{contestId}/image/{imageId}")
    public String showSingleImage(@PathVariable int contestId, @PathVariable int imageId,
                                  HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Contest contest = contestService.getById(contestId);
            Image image = imageService.getById(imageId);
            model.addAttribute("user", user);
            model.addAttribute("contest", contest);
            model.addAttribute("image", image);
            model.addAttribute("newReview", new ReviewDTO());
            if (contest.getPhase().equals(ContestPhase.FINISHED)) {
                model.addAttribute("evaluations",
                        contestService.getAllContestImageEvaluations(contest.getId()));
            }
            return "image";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{contestId}/uploadimage")
    public String uploadImagePage(@PathVariable int contestId, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Contest contest = contestService.getById(contestId);
            model.addAttribute("contest", contest);
            model.addAttribute("image", new ImageDTO());
            return "image-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/{contestId}/uploadimage")
    public String uploadFile(@PathVariable int contestId, @RequestParam("file") MultipartFile file,
                             @Valid @ModelAttribute("image") ImageDTO imageDTO, BindingResult errors,
                             Model model,
                             HttpSession session, RedirectAttributes attributes) throws IOException {
        if (errors.hasErrors()) {
            return "image-new";
        }
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (file.isEmpty()) {
            attributes.addFlashAttribute("message", "Please select a file to upload.");
            return "image-new";
        }

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        Path path;
        try {
            path = Paths.get(UPLOADED_FOLDER + fileName);
            Image image = imageMapper.fromDto(imageDTO, user);
            image.setImageAbsolutePath(path.toString());
            imageService.create(image, contestId);
        } catch (DuplicateEntityException e) {
            errors.rejectValue("title", "duplicate_image", e.getMessage());
            return "image-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UserAlreadyParticipatingException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/contests/{contestId}";
        }
        Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        return "redirect:/contests/{contestId}";
    }

    @PostMapping("/{contestId}/image/{imageId}/delete")
    public String deleteImage(@PathVariable int contestId, @PathVariable int imageId,
                              Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            imageService.delete(contestId, imageId, user);
            return "redirect:/contests/{contestId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/{contestId}/image/{imageId}/review")
    public String reviewImage(@PathVariable int contestId, @PathVariable int imageId,
                              ReviewDTO reviewDTO, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Contest contest = contestService.getById(contestId);
            model.addAttribute("newReview", reviewDTO);
            if (!reviewDTO.isValid()) {
                Review reviewNotValid = new Review(0, NOT_RELEVANT);
                reviewService.reviewImage(reviewNotValid, imageId, user, contestId);
            } else {
                Review review = new Review(reviewDTO.getScore(), reviewDTO.getComment());
                reviewService.reviewImage(review, imageId, user, contestId);
            }
            return "redirect:/contests/{contestId}/image/{imageId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{contestId}/invite")
    public String inviteJunkiesPage(@PathVariable int contestId, Model model,
                                    HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Contest contest = contestService.getById(contestId);
            model.addAttribute("contest", contest);
            model.addAttribute("inviteDto", new ContestInviteJunkiesDTO());
            return "contest-invite-junkies";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/{contestId}/invite")
    public String inviteJunkiesPage(@PathVariable int contestId,
                                    ContestInviteJunkiesDTO contestInviteJunkiesDTO,
                                    Model model,
                                    HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Contest contest = contestService.getById(contestId);
            model.addAttribute("contest", contest);
            model.addAttribute("inviteDto", contestInviteJunkiesDTO);
            contestService.inviteJunkies(contestInviteJunkiesDTO.getInvitedJunkies(), contestId);
            return "redirect:/contests/{contestId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }
}