package com.photocontest.controllers.mvc;

import com.photocontest.exceptions.AuthenticationFailureException;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.Image;
import com.photocontest.models.daos.ImageEvaluationDao;
import com.photocontest.services.contracts.ContestService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeMVCController {

    private final ContestService contestService;
    public HomeMVCController(ContestService contestService) {
        this.contestService = contestService;
    }

    @ModelAttribute("isAuthorized")
    public boolean populateIsAuthorized(HttpSession session) {
        if (!(getUserId(session).equals(-1))) {
            return session.getAttribute("isOrganizer").equals(true);
        }
        return false;
    }

    @ModelAttribute("userID")
    public Object getUserId(HttpSession session) {
        if (session.getAttribute("userId") != null) {
            return session.getAttribute("userId");
        }
        return -1;
    }

    @ModelAttribute("evaluationsFirstPlace")
    public List<Image> getevaluations() {
        List<ImageEvaluationDao> sorted = contestService.getTopPhotos();
        Collections.shuffle(sorted);
        List<Image> imagesToDisplay = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            imagesToDisplay.add(sorted.get(i).getImage());
        }
        return imagesToDisplay;
    }

    @GetMapping
    public String showHomePage(Model model) {
        try {
            return "index";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }
}
