package com.photocontest.controllers.mvc;

import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.Category;
import com.photocontest.services.contracts.CategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/categories")
public class CategoryMVCController {

    private final CategoryService categoryService;

    public CategoryMVCController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @ModelAttribute("isAuthorized")
    public boolean populateIsAuthorized(HttpSession session) {
        if (!(getUserId(session).equals(-1))) {
            return session.getAttribute("isOrganizer").equals(true);
        }
        return false;
    }

    @ModelAttribute("userID")
    public Object getUserId(HttpSession session) {
        if (session.getAttribute("userId") != null) {
            return session.getAttribute("userId");
        }
        return -1;
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @GetMapping
    public String showAllContests(Model model) {
        List<Category> categories = categoryService.getAll();
        model.addAttribute("categories", categories);
//        model.addAttribute("filterContestDto", new FilterContestDTO());
        return "categories";
    }

    @GetMapping("/{id}")
    public String showSingleContest(@PathVariable int id, Model model) {
        try {
            Category category = categoryService.getById(id);
            model.addAttribute("category", category);

            return "category";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
}
