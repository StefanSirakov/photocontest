package com.photocontest.controllers.mvc;

import com.photocontest.controllers.helpers.Utility;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.models.User;
import com.photocontest.services.contracts.UserService;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

@Controller
public class ForgotPasswordMVCController {

    private final JavaMailSender mailSender;
    private final UserService userService;

    @Autowired
    public ForgotPasswordMVCController(JavaMailSender mailSender, UserService userService) {
        this.mailSender = mailSender;
        this.userService = userService;
    }

    @GetMapping("/forgot_password")
    public String showForgotPasswordForm() {
        return "forgot-password-form";
    }

    @PostMapping("/forgot_password")
    public String processForgotPassword(HttpServletRequest request, Model model) {
        String email = request.getParameter("email");
        String token = RandomString.make(30);

        try {
            userService.updateResetPasswordToken(token, email);
            String resetPasswordLink = Utility.getSiteURL(request) + "/reset_password?token=" + token;
            sendEmail(email, resetPasswordLink);
            model.addAttribute("message", "We have sent a reset password link to your email. Please check.");

        } catch (EntityNotFoundException ex) {
            model.addAttribute("error", ex.getMessage());
        } catch (UnsupportedEncodingException | MessagingException e) {
            model.addAttribute("error", "Error while sending email");
        }

        return "forgot-password-form";
    }

    public void sendEmail(String recipientEmail, String link)
            throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("bestshot3355@gmail.com", "Best Shot Support");
        helper.setTo(recipientEmail);

        String subject = "Here's the link to reset your password";

        String content = "<p>Hello,</p>"
                + "<p>You have requested to reset your password.</p>"
                + "<p>Click the link below to change your password:</p>"
                + "<p><a href=\"" + link + "\">Change my password</a></p>"
                + "<br>"
                + "<p>Ignore this email if you do remember your password, "
                + "or you have not made the request.</p>";

        helper.setSubject(subject);

        helper.setText(content, true);

        mailSender.send(message);
    }


    @GetMapping("/reset_password")
    public String showResetPasswordForm(@Param(value = "token") String token, Model model) {
        try {
            userService.getUserByPasswordResetToken(token);
            model.addAttribute("token", token);
            return "reset-password-form";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", "Invalid Token.");
            return "forgot-password-form";
        }
    }

    @PostMapping("/reset_password")
    public String processResetPassword(HttpServletRequest request, Model model) {
        String token = request.getParameter("token");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirmPassword");

        if(!(password.equals(confirmPassword))) {
            model.addAttribute("error", "Passwords do not match.");
            return "redirect:/reset_password?token=" + token;
        }

        try{
            User user = userService.getUserByPasswordResetToken(token);
            model.addAttribute("title", "Reset your password");
            userService.changeUserPassword(user, password);
            model.addAttribute("message", "You have successfully changed your password.");
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", "Invalid Token");
            return "reset-password-form";
        }
    }
}
