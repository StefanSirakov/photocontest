package com.photocontest.controllers.mvc;

import com.photocontest.controllers.helpers.AuthenticationHelper;
import com.photocontest.exceptions.AuthenticationFailureException;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.mappers.UserMapper;
import com.photocontest.models.Category;
import com.photocontest.models.Contest;
import com.photocontest.models.DTOs.*;
import com.photocontest.models.Image;
import com.photocontest.models.User;
import com.photocontest.models.enums.ContestPhase;
import com.photocontest.models.enums.Rank;
import com.photocontest.services.contracts.CategoryService;
import com.photocontest.services.contracts.ContestService;
import com.photocontest.services.contracts.ImageService;
import com.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/users")
public class UserMVCController {

    private static final String UPLOADED_FOLDER = "C:\\photo-contest\\src\\main\\resources\\static\\assets\\avatars\\";

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;
    private final ContestService contestService;
    private final ImageService imageService;
    private final CategoryService categoryService;


    @Autowired
    public UserMVCController(UserService userService, AuthenticationHelper authenticationHelper, UserMapper userMapper,
                             ContestService contestService, ImageService imageService, CategoryService categoryService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.contestService = contestService;
        this.imageService = imageService;
        this.categoryService = categoryService;
    }

    @ModelAttribute("isAuthorized")
    public boolean populateIsAuthorized(HttpSession session) {
        if (!(getUserId(session).equals(-1))) {
            return session.getAttribute("isOrganizer").equals(true);
        }
        return false;
    }

    @ModelAttribute("userID")
    public Object getUserId(HttpSession session) {
        if (session.getAttribute("userId") != null) {
            return session.getAttribute("userId");
        }
        return -1;
    }

    @ModelAttribute("userRanks")
    public Rank[] fetchAllUserRanks() {
        return Rank.values();
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("phases")
    public ContestPhase[] getAllPhases() {
        return ContestPhase.values();
    }

    @PostMapping()
    public String filterUsers(@ModelAttribute("filterUserDTO") FilterUserDTO filterUserDTO, Model model) {

        var filtered = userService.filter(
                Optional.ofNullable(filterUserDTO.getSearch().isBlank() ? null : filterUserDTO.getSearch()),
                Optional.ofNullable(filterUserDTO.getMinPoints() == null ? null : filterUserDTO.getMinPoints()),
                Optional.ofNullable(filterUserDTO.getRole() == null ? null : filterUserDTO.getRole())
        );
        model.addAttribute("users", filtered);
        return "users";
    }

    @GetMapping()
    public String showAllUsers(Model model, HttpSession session) {
        User userToCheck;
        try {
            userToCheck = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        List<User> users = userService.getAllOrderedByPoints();
        model.addAttribute("filterUserDTO", new FilterUserDTO());
        model.addAttribute("users", users);
        return "users";
    }

    @GetMapping("/{id}")
    public String showSingleUser(@PathVariable int id, Model model, HttpSession session) {
        User userToCheck;
        try {
            userToCheck = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User user = userService.getById(id);
            model.addAttribute("user", user);
            return "user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditUserProfilePage(@PathVariable int id, Model model, HttpSession session) {

        User userFromSession;
        User userProfile;

        try {
            userFromSession = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            userProfile = userService.getById(id);
            if (userFromSession.getId() != userProfile.getId()) {
                return "access-denied";
            }
            UserProfileUpdateDTO dto = userMapper.objectToDTO(userProfile);
            model.addAttribute("userDTO", dto);

            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUserProfile(@PathVariable int id,
                                    @ModelAttribute("userDTO") UserProfileUpdateDTO dto,
                                    BindingResult errors,
                                    Model model,
                                    HttpSession session) {

        User userFromSession;
        try {
            userFromSession = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "user-update";
        }


        try {
            User userToBeUpdated = userMapper.dtoToObjectForUpdate(dto, id);

            userService.update(userFromSession, userToBeUpdated);

            return "redirect:/users/{id}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("{id}/change-password")
    public String showChangePasswordPage(@PathVariable int id, Model model, HttpSession session) {
        User userFromSession;
        User userProfile;

        try {
            userFromSession = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            userProfile = userService.getById(id);
            if (userFromSession.getId() != userProfile.getId()) {
                return "access-denied";
            }
            model.addAttribute("passwordDTO", new PasswordUpdateDTO());

            return "password-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("{id}/change-password")
    public String updatePassword(@PathVariable int id,
                                    @ModelAttribute("passwordDTO") PasswordUpdateDTO dto,
                                    BindingResult errors,
                                    Model model,
                                    HttpSession session) {

        User userFromSession;
        try {
            userFromSession = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "user-update";
        }

        try {
            userService.updatePassword(userFromSession, id, dto);
            session.removeAttribute("currentUser");
            session.removeAttribute("userID");

            return "redirect:/users/{id}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "password-update";
        }
    }

    @GetMapping("{id}/change-avatar")
    public String showChangeAvatarPage(@PathVariable int id, Model model, HttpSession session) {
        User userFromSession;
        User userProfile;

        try {
            userFromSession = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            userProfile = userService.getById(id);
            if (userFromSession.getId() != userProfile.getId()) {
                return "access-denied";
            }
            model.addAttribute("user", userProfile);
            return "avatar-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("{id}/change-avatar")
    public String changeAvatar(@PathVariable int id, @RequestParam("file") MultipartFile file,
                               @Valid @ModelAttribute("avatar") AvatarDTO avatarDTO, BindingResult errors,
                               Model model,
                               HttpSession session, RedirectAttributes attributes) throws IOException {
        if (errors.hasErrors()) {
            return "avatar-update";
        }
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (file.isEmpty()) {
            attributes.addFlashAttribute("message", "Please select a file to upload.");
            return "avatar-update";
        }

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        Path path;
        try {

            path = Paths.get(UPLOADED_FOLDER + fileName);
            user.setAvatarAbsPath(path.toString());

            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);

            userService.update(user, user);
            return "redirect:/users/{id}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession session) {
        User userFromSession;
        try {
            userFromSession = authenticationHelper.tryGetUser(session);
            User userToBeDeleted = userService.getById(id);

            model.addAttribute("user", userToBeDeleted);

            userService.delete(id, userFromSession);

            session.removeAttribute("userId");
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }
    @PostMapping("/{id}/promote")
    public String promoteUserToOrganizer(@PathVariable int id,
                                         Model model,
                                         HttpSession session) {
        User userFromSession;
        try {
            userFromSession = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User userToBePromoted = userService.getById(id);
            userService.promoteToOrganizer(userToBePromoted, userFromSession);
            return "redirect:/users/{id}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{userId}/contests")
    public String showAllUserContests(@PathVariable int userId, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "login";
        }

        User userToView = userService.getById(userId);
        List<Contest> contests = contestService.getByUserEntry(user.getId());
        model.addAttribute("userContests", contests);
        model.addAttribute("userToView", userToView);
        model.addAttribute("filterContestDto", new FilterContestDTO());
        return "user-contests";
    }

    @PostMapping("/{userId}/contests")
    public String filterUserContests(@PathVariable int userId,
                                     @ModelAttribute("filterContestDto") FilterContestDTO filterContestDto, Model model) {
        var filtered = contestService.filter(
                Optional.ofNullable(filterContestDto.getTitle().isBlank() ? null : filterContestDto.getTitle()),
                Optional.ofNullable(filterContestDto.getCategoryId() == null ? null : filterContestDto.getCategoryId()),
                Optional.ofNullable(filterContestDto.getPhase().isBlank() ? null : filterContestDto.getPhase())
        );
        if (filtered.size() > 0) {
            List<Contest> userContests = filtered.stream().filter(contest -> contest.isUserParticipating(userId)).collect(Collectors.toList());
            model.addAttribute("userContests", userContests);
        } else {
            List<Contest> userContests = contestService.getByJuror(userId);
            model.addAttribute("userContests", userContests);
        }
        return "user-contests";
    }

    @GetMapping("/{userId}/images")
    public String showAllUserImages(@PathVariable int userId, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "login";
        }

        User userToView = userService.getById(userId);
        List<Image> images = imageService.getByUserId(user.getId());
        model.addAttribute("userImages", images);
        model.addAttribute("userToView", userToView);
        return "user-images";
    }

    @GetMapping("/{userId}/contestsasjuror")
    public String showAllUserContestsAsJuror(@PathVariable int userId, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "login";
        }

        List<Contest> contestsAsJuror = contestService.getByJuror(user.getId());
        model.addAttribute("contestsAsJuror", contestsAsJuror);
        model.addAttribute("filterContestDto", new FilterContestDTO());
        model.addAttribute("user", user);
        return "user-contests-juror";
    }

    @PostMapping("/{userId}/contestsasjuror")
    public String filterUserContestsAsJuror(@PathVariable int userId,
                                            @ModelAttribute("filterContestDto") FilterContestDTO filterContestDto, Model model) {
        var filtered = contestService.filter(
                Optional.ofNullable(filterContestDto.getTitle().isBlank() ? null : filterContestDto.getTitle()),
                Optional.ofNullable(filterContestDto.getCategoryId() == null ? null : filterContestDto.getCategoryId()),
                Optional.ofNullable(filterContestDto.getPhase().isBlank() ? null : filterContestDto.getPhase())
        );
        if (filtered.size() > 0) {
            List<Contest> contestsAsJuror = filtered.stream().filter(contest -> contest.isUserJuror(userId)).collect(Collectors.toList());
            model.addAttribute("contestsAsJuror", contestsAsJuror);
        } else {
            List<Contest> contestsAsJuror = contestService.getByJuror(userId);
            model.addAttribute("contestsAsJuror", contestsAsJuror);
        }
        return "user-contests-juror";
    }
}
