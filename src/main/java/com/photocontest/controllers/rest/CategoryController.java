package com.photocontest.controllers.rest;

import com.photocontest.controllers.helpers.AuthenticationHelper;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.models.Category;
import com.photocontest.models.User;
import com.photocontest.services.contracts.CategoryService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {

    private final CategoryService categoryService;
    private final AuthenticationHelper authenticationHelper;

    public CategoryController(CategoryService categoryService, AuthenticationHelper authenticationHelper) {
        this.categoryService = categoryService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Category> getAll(@RequestHeader HttpHeaders headers,
                                 @RequestParam(required = false) Optional<String> search){
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return categoryService.search(search);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Category getById(@RequestHeader HttpHeaders headers,
                            @PathVariable int id){
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return categoryService.getById(id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
