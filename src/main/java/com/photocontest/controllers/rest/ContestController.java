package com.photocontest.controllers.rest;

import com.photocontest.controllers.helpers.AuthenticationHelper;
import com.photocontest.exceptions.DuplicateEntityException;
import com.photocontest.exceptions.EntityNotFoundException;
import com.photocontest.exceptions.UnauthorizedOperationException;
import com.photocontest.mappers.ContestMapper;
import com.photocontest.mappers.ImageMapper;
import com.photocontest.mappers.ReviewMapper;
import com.photocontest.models.Contest;
import com.photocontest.models.DTOs.ContestDTO;
import com.photocontest.models.DTOs.ReviewDTO;
import com.photocontest.models.Image;
import com.photocontest.models.Review;
import com.photocontest.models.User;
import com.photocontest.services.contracts.ContestService;
import com.photocontest.services.contracts.ImageService;
import com.photocontest.services.contracts.ReviewService;
import com.photocontest.services.contracts.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/contests")
public class ContestController {

    private final ImageMapper imageMapper;
    private final ImageService imageService;
    private final ContestService contestService;
    private final UserService userService;
    private final ReviewService reviewService;
    private final AuthenticationHelper authenticationHelper;
    private final ContestMapper contestMapper;
    private final ReviewMapper reviewMapper;

    private static String UPLOADED_FOLDER = "C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\";
    public static final String IMAGE_RATED = "Image rated - %d points";

    public ContestController(ContestService contestService, ImageMapper imageMapper,
                             ImageService imageService, UserService userService1,
                             ReviewService reviewService, AuthenticationHelper authenticationHelper,
                             ContestMapper contestMapper, ReviewMapper reviewMapper) {
        this.contestService = contestService;
        this.imageMapper = imageMapper;
        this.imageService = imageService;
        this.userService = userService1;
        this.reviewService = reviewService;
        this.authenticationHelper = authenticationHelper;
        this.contestMapper = contestMapper;
        this.reviewMapper = reviewMapper;
    }

    @GetMapping
    public List<Contest> getAll(@RequestHeader HttpHeaders headers,
                                @RequestParam(required = false) Optional<String> search) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return contestService.search(search);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<Contest> filter(@RequestParam(required = false) Optional<String> title,
                                @RequestParam(required = false) Optional<Integer> categoryId,
                                @RequestParam(required = false) Optional<String> phase) {
        try {
            return contestService.filter(title, categoryId, phase);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Contest getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return contestService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public Contest create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ContestDTO contestDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Contest contest = contestMapper.dtoToObject(contestDTO);
            String categoryName = contestDTO.getCategory();
            contestService.create(contest, categoryName, user);
            return contest;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Contest update(@RequestHeader HttpHeaders headers, @PathVariable int id,
                          @Valid @RequestBody ContestDTO contestDTO) {
        try {
            User userToAuthenticate = authenticationHelper.tryGetUser(headers);
            Contest contestToUpdate = contestMapper.fromDto(contestDTO, id);
            contestService.update(contestToUpdate, userToAuthenticate);
            return contestToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            contestService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{contestId}/uploadimage")
    public String singleFileUpload(@PathVariable int contestId,
                                   @RequestParam("title") @Min(value = 3) @Max(value = 30) String title,
                                   @RequestParam("story") @Min(value = 10) @Max(value = 1000) String story,
                                   @RequestPart @NotBlank MultipartFile file) {
        try {
            User user = userService.getById(9);
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Image image = imageMapper.fileToObject(contestId, user, path, title, story);
            imageService.create(image, contestId);

            File serverFile = new File(path + File.separator);
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
            stream.write(bytes);
            stream.close();

            return "Successful upload.";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{contestId}/{imageId}")
    public String deleteImageFromContest(@RequestHeader HttpHeaders headers,
                                         @PathVariable int contestId,
                                         @PathVariable int imageId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Contest contest = contestService.getById(contestId);
            Image image = imageService.getById(imageId);

            File file = new File(image.getImageAbsolutePath());
            file.delete();

            imageService.delete(contestId, imageId, user);

            return "Successful delete.";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/image/{imageId}")
    public File getImageById(@PathVariable int imageId) {
        try {
//            User user = authenticationHelper.tryGetUser(headers);
            User user = userService.getById(9);
            Image image = imageService.getById(imageId);
            File file = new File(image.getImageAbsolutePath());
            return file;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{contestId}/image/{imageId}/rate")
    public String rateImage(@PathVariable int contestId, @PathVariable int imageId,
                            @RequestHeader HttpHeaders headers,
                            @RequestBody ReviewDTO reviewDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Review review = reviewMapper.dtoToObject(reviewDTO, user);
            reviewService.reviewImage(review, imageId, user, contestId);
            return String.format(IMAGE_RATED, review.getScore());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{contestId}/photos")
    public List<Image> getAllPhotos(@PathVariable int contestId,
                                    @RequestHeader HttpHeaders headers,
                                    @RequestParam(required = false) Optional<String> search) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return imageService.searchPhoto(search, user, contestId);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
