package com.photocontest.schedulingtasks;

import com.photocontest.services.contracts.ContestService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class ScheduledTasks {

    private final ContestService contestService;

    public ScheduledTasks(ContestService contestService) {
        this.contestService = contestService;
    }

    @Scheduled(fixedRate = 5000)
    public void checkPhaseTime() {
        contestService.checkPhase();
    }
}
