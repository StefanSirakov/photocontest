package com.photocontest.exceptions;

public class ContestAlreadyFinishedException extends RuntimeException {
    public ContestAlreadyFinishedException(String type, String title) {
        super(String.format("%s - '%s' has already ended.", type, title));
    }
    public ContestAlreadyFinishedException(String message) {
        super(message);
    }
}
