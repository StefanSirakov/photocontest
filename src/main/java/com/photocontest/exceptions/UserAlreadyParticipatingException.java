package com.photocontest.exceptions;

public class UserAlreadyParticipatingException extends RuntimeException {
    public UserAlreadyParticipatingException(String message) {
        super(message);
    }
}
