package com.photocontest.models;

import javax.persistence.*;

@Entity
@Table(name = "contest_winners")
public class ContestWinner {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne
    @JoinColumn(name = "contest_id")
    private Contest contest;

    @OneToOne
    @JoinColumn(name = "image_id")
    private Image image;

    @Column(name = "place")
    private int place;

    public ContestWinner(Contest contest, Image image, int place) {
        this.contest = contest;
        this.image = image;
        this.place = place;
    }

    public ContestWinner() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

}

