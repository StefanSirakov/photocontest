package com.photocontest.models.daos;

import com.photocontest.models.Image;
import com.photocontest.models.Review;

import java.util.Set;

public class ImageEvaluationDao {

    private Image image;

    private Set<Review> reviews;

    private int score;

    private int rankingPlace;


    public ImageEvaluationDao() {
    }
    public ImageEvaluationDao(Image image, Set<Review> reviews, int score, int rankingPlace) {
        this.image = image;
        this.reviews = reviews;
        this.score = score;
        this.rankingPlace = rankingPlace;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getRankingPlace() {
        return rankingPlace;
    }

    public String getRankingPlaceAsString() {
        if (rankingPlace == 1) return "First!";
        if (rankingPlace == 2) return "Second!";
        if (rankingPlace == 3) return "Third!";
        return "Not Ranked!";
    }

    public void setRankingPlace(int rankingPlace) {
        this.rankingPlace = rankingPlace;
    }
}

