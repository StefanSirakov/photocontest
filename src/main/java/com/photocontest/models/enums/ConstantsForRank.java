package com.photocontest.models.enums;

public class ConstantsForRank {

    public static final int JUNKIE_RANK_STARTING_POINTS = 0;
    public static final int ENTHUSIAST_RANK_STARTING_POINTS = 51;
    public static final int MASTER_RANK_STARTING_POINTS = 151;
    public static final int WBPD_RANK_STARTING_POINTS = 1001;
}
