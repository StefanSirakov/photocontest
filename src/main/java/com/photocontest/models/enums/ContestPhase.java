package com.photocontest.models.enums;

public enum ContestPhase {

    PHASE_I("Phase I"),
    PHASE_II("Phase II"),
    FINISHED("Finished");

    String phaseAsString;

    ContestPhase(String phaseAsString) {
        this.phaseAsString = phaseAsString;
    }

    @Override
    public String toString() {
        return this.phaseAsString;
    }
}
