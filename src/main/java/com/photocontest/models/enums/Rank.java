package com.photocontest.models.enums;

import static com.photocontest.models.enums.ConstantsForRank.*;

public enum Rank {
    JUNKIE(JUNKIE_RANK_STARTING_POINTS, "Junkie"),
    ENTHUSIAST(ENTHUSIAST_RANK_STARTING_POINTS, "Enthusiast"),
    MASTER(MASTER_RANK_STARTING_POINTS, "Master"),
    WISE_AND_BENEVOLENT_PHOTO_DICTATOR(WBPD_RANK_STARTING_POINTS, "WBPhD");

    int minPoints;
    String rankAsString;

    Rank(int rankStartingPoints, String rankAsString) {
        this.minPoints = rankStartingPoints;
        this.rankAsString = rankAsString;
    }

    public static Rank getRankFor(int rankingPoints) {
        if (rankingPoints < ENTHUSIAST_RANK_STARTING_POINTS) return Rank.JUNKIE;
        if (rankingPoints < MASTER_RANK_STARTING_POINTS) return Rank.ENTHUSIAST;
        if (rankingPoints < WBPD_RANK_STARTING_POINTS) return Rank.MASTER;
        return Rank.WISE_AND_BENEVOLENT_PHOTO_DICTATOR;
    }

    public String getPointsTillNextRank(int currentPoints) {
        if (this == WISE_AND_BENEVOLENT_PHOTO_DICTATOR) return "Already at max rank";
        return String.valueOf(values()[this.ordinal() + 1].minPoints - currentPoints);
    }

    public int getRankMinPoints() {
        return this.minPoints;
    }

    @Override
    public String toString() {
        return this.rankAsString;
    }
}
