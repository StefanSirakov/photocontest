package com.photocontest.models;

import javax.persistence.*;

@Entity
@Table(name = "images_reviews")
public class ImagesReviews {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "image_id")
    private int imageId;

    @Column(name = "review_id")
    private int reviewId;

    public ImagesReviews() {
    }

    public ImagesReviews(int imageId, int reviewId) {
        this.imageId = imageId;
        this.reviewId = reviewId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }
}
