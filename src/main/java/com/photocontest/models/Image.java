package com.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "images")
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "image")
    private String imageAbsPath;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "title")
    private String title;

    @Column(name = "story")
    private String story;

    @Column(name = "contest_id")
    private int contestId;

    @Column(name = "total_score")
    private int totalScore;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "images_reviews",
            joinColumns = @JoinColumn(name = "image_id"),
            inverseJoinColumns = @JoinColumn(name = "review_id")
    )
    private Set<Review> reviews;

    @JsonIgnore
    public boolean isEvaluatedBy(int userId) {
        return reviews.stream().anyMatch(review -> review.getJuror().getId() == userId);
    }

    public Image() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageAbsolutePath() {
        return imageAbsPath;
    }

    public void setImageAbsolutePath(String imageAbsPath) {
        this.imageAbsPath = imageAbsPath;
    }

    public String getImagePath() {
        String imagePath = imageAbsPath.substring(42);
        imagePath.replace("\\", "/");
        return imagePath;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public void addPoints(int points) {
        this.totalScore += points;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }


}
