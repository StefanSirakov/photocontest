package com.photocontest.models;

import javax.persistence.*;

@Entity
@Table(name = "contests_jurors")
public class ContestsJurors {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne
    @JoinColumn(name = "juror_id")
    private User juror;

    @OneToOne
    @JoinColumn(name = "contest_id")
    private Contest contest;

    public ContestsJurors() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getJuror() {
        return juror;
    }

    public void setJuror(User juror) {
        this.juror = juror;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }
}
