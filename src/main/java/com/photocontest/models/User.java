package com.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.photocontest.models.enums.Rank;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "is_organiser")
    private boolean isOrganiser;

    @OneToOne
    @JoinColumn(name = "rank_id")
    private UserRank rank;

    @Column(name = "points")
    private int points;

    @JoinColumns({
            @JoinColumn(name = "user_id")
    })
    @OneToMany(fetch = FetchType.EAGER)
    private Set<Image> images;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @Column(name = "reset_password_token")
    private String resetPasswordToken;

    @Column(name = "avatar")
    private String avatarAbsPath;

//    private Set<Role> roles;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isOrganiser() {
        return isOrganiser;
    }

    public void setOrganiser(boolean organiser) {
        isOrganiser = organiser;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void addPoints(int points) {
        this.points += points;
    }

    public Rank getRank() {
        return Rank.getRankFor(this.points);
    }

    public void setUserRank(UserRank rank) {
        this.rank = rank;
    }

    public UserRank getUserRank() {
        return this.rank;
    }

    public Set<Image> getImages() {
        return images;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getResetPasswordToken() {
        return resetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }

    public String getAvatarAbsPath() {
        return avatarAbsPath;
    }

    public void setAvatarAbsPath(String avatarAbsPath) {
        this.avatarAbsPath = avatarAbsPath;
    }

    public String getAvatarRelPath() {
        String imagePath = avatarAbsPath.substring(42);
        imagePath.replace("\\", "/");
        return imagePath;
    }

    public String getFirstAndLastName() {
        return firstName + " " + lastName;
    }

    public String getRankInfo() {
        return "Rank: " + rank.getName();
    }

    public String getPointsInfo() {
        if(rank.getId()==4) {
            return String.format("Points: %d", points);
        } else {
            int pointsToNextRank = pointsToNextRank();
            return String.format("Points: %d (%d points to next rank.)",
                    points, pointsToNextRank);
        }
    }

    public int pointsToNextRank() {
        if(this.rank.getId()==3) {
            return 1000 - points;
        } else if(this.rank.getId() == 2) {
            return 150 - points;
        } else {
            return 50 - points;
        }
    }

    @JsonIgnore
    public boolean isEligibleForJuror() {
        return isOrganiser() || getRank() == Rank.MASTER || getRank() == Rank.WISE_AND_BENEVOLENT_PHOTO_DICTATOR;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return username.equals(user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }

}