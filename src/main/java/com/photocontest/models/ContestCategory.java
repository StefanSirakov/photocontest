package com.photocontest.models;

import javax.persistence.*;

@Entity
@Table(name = "contests_categories")
public class ContestCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "contest_id")
    private int contestId;

    @Column(name = "category_id")
    private int categoryId;

    public ContestCategory() {
    }

    public ContestCategory(int contestId, int categoryId) {
        this.contestId = contestId;
        this.categoryId = categoryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
