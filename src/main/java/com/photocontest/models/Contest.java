package com.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.photocontest.models.enums.ContestPhase;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Set;

@Entity
@Table(name = "contests")
public class Contest {

    public static final String PHASE_2_STARTED = "Phase II started!";
    public static final String CONTEST_HAS_ENDED = "Contest has ended!";
    public static final String TIME_UNTIL_PHASE_2 = "Time until phase II starts: ";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "title")
    private String title;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User creator;

    @Column(name = "is_open")
    private boolean isOpen;

    @Column(name = "phase")
    private int phase;

    @Column(name = "contest_starting_time")
    private LocalDateTime contest_starting_time;

    @Column(name = "phase_two_starting_time")
    private LocalDateTime phase_two_starting_time;

    @Column(name = "contest_ending_time")
    private LocalDateTime contest_ending_time;
    @Column(name = "background_image")
    private String backgroundImageAbsPath;

    @JsonIgnore
    @JoinColumns({
            @JoinColumn(name = "contest_id")
    })
    @OneToMany(fetch = FetchType.EAGER)
    private Set<Image> images;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "contests_jurors",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "juror_id")
    )
    private Set<User> jurry;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "contest_participants",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> participants;


    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "contest_invited_junkies",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> invitedJunkies;

    public Contest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User user) {
        this.creator = user;
    }

    public boolean getIsOpen() {
        return isOpen;
    }

    public String getIsOpenAsString() {
        if (isOpen) return "Open";
        return "Invitational";
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public ContestPhase getPhase() {
        if (LocalDateTime.now().isBefore(phase_two_starting_time)) return ContestPhase.PHASE_I;
        if (LocalDateTime.now().isBefore(contest_ending_time)) return ContestPhase.PHASE_II;
        return ContestPhase.FINISHED;
    }

    public String getPhaseAsString() {
        if (this.phase == 1) return "I";
        else if (this.phase == 2) return "II";
        else return "Finished";
    }

    public void setPhase(int phase) {
        this.phase = phase;
    }

    public LocalDateTime getContest_starting_time() {
        return contest_starting_time;
    }

    public void setContest_starting_time(LocalDateTime contestStartingTime) {
        this.contest_starting_time = contestStartingTime;
    }

    public LocalDateTime getPhaseTwoStartingTime() {
        return phase_two_starting_time;
    }

    public void setPhaseTwoStartingTime(LocalDateTime phaseTwoStartingTime) {
        this.phase_two_starting_time = phaseTwoStartingTime;
    }

    public LocalDateTime getContestEndingTime() {
        return contest_ending_time;
    }

    public void setContestEndingTime(LocalDateTime contentEndingTime) {
        this.contest_ending_time = contentEndingTime;
    }

    public Set<Image> getImages() {
        return images;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

    public int getParticipantsSize() {
        return participants.size();
    }

    public String getBackgroundImagePath() {
        String backgroundImagePath = backgroundImageAbsPath.substring(42);
        backgroundImagePath.replace("\\", "/");
        return backgroundImagePath;
    }

    public Set<User> getJurry() {
        return jurry;
    }

    public void setJurry(Set<User> jurry) {
        this.jurry = jurry;
    }

    public Set<User> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<User> participants) {
        this.participants = participants;
    }

    public String getBackgroundImageAbsPath() {
        return backgroundImageAbsPath;
    }

    public void setBackgroundImageAbsPath(String backgroundImageAbsPath) {
        this.backgroundImageAbsPath = backgroundImageAbsPath;
    }

    public Set<User> getInvitedJunkies() {
        return invitedJunkies;
    }

    public void setInvitedJunkies(Set<User> invitedJunkies) {
        this.invitedJunkies = invitedJunkies;
    }

    public String getTimeRemainingTillPhase2() {
        LocalDateTime now = LocalDateTime.now();

        if (now.isBefore(phase_two_starting_time)) {
            long days = now.until(phase_two_starting_time, ChronoUnit.DAYS);
            now = now.plusDays(days);
            long hours = now.until(phase_two_starting_time, ChronoUnit.HOURS);
            now = now.plusHours(hours);
            long minutes = now.until(phase_two_starting_time, ChronoUnit.MINUTES);
            now = now.plusMinutes(minutes);

            return TIME_UNTIL_PHASE_2 +  days + " days " + hours + " hours " + minutes + " minutes ";
        } else if (now.isBefore(contest_ending_time)){
            return PHASE_2_STARTED;
        } else {
            return CONTEST_HAS_ENDED;
        }
    }

    public boolean isUserParticipating(int userId) {
        return participants.stream().anyMatch(user -> user.getId() == userId);
    }

    public boolean isUserInvited(int userId) {
        return invitedJunkies.stream().anyMatch(user -> user.getId() == userId);
    }

    public boolean isUserJuror(int userId) {
        return jurry.stream().anyMatch(user ->  user.getId() == userId);
    }

    public boolean isUserJurorOrParticipating(User user) {
        if (participants.contains(user)) return true;
        if (jurry.contains(user)) return true;
        return false;
    }
    public boolean isInPhaseOne() {
        return phase == 1;
    }

    public boolean isInPhaseTwo() {
        return phase == 2;
    }

    public boolean isFinished() {
        return phase == 3;
    }

}
