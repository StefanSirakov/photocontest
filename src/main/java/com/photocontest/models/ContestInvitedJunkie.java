package com.photocontest.models;

import javax.persistence.*;

@Entity
@Table(name = "contest_invited_junkies")
public class ContestInvitedJunkie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "contest_id")
    private int contestId;

    @Column(name = "user_id")
    private int userId;

    public ContestInvitedJunkie() {
    }

    public ContestInvitedJunkie(int contestId, int userId) {
        this.contestId = contestId;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
