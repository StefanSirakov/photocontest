package com.photocontest.models.DTOs;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ContestDTO {

    @NotNull(message = "Title should not be null.")
    @Size(message = "The title must be between 4 and 64 symbols.")
    private String title;

    @NotNull(message = "Category should not be null.")
    @Size(message = "Category name must be between 4 and 20 symbols")
    private String category;

    @NotNull(message = "Join type should not be null")
    private boolean isOpen;

    @NotNull(message = "Phase 2 time should not be null")
    private String phaseTwoStartingTime;

    @NotNull(message = "Contest ending time should not be null")
    private String contestEndingTime;

    private MultipartFile backgroundImage;

    private Integer[] addedJurors;

    public ContestDTO() {
    }

    public ContestDTO(String title, String category, boolean isOpen, String phaseTwoStartingTime, String contentEndingTime) {
        this.title = title;
        this.category = category;
        this.isOpen = isOpen;
        this.phaseTwoStartingTime = phaseTwoStartingTime;
        this.contestEndingTime = contentEndingTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean open) {
        this.isOpen = open;
    }

    public String getPhaseTwoStartingTime() {
        return phaseTwoStartingTime;
    }

    public void setPhaseTwoStartingTime(String phaseTwoStartingTime) {
        this.phaseTwoStartingTime = phaseTwoStartingTime;
    }

    public String getContestEndingTime() {
        return contestEndingTime;
    }

    public void setContestEndingTime(String contestEndingTime) {
        this.contestEndingTime = contestEndingTime;
    }

    public MultipartFile getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(MultipartFile backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public Integer[] getAddedJurors() {
        return addedJurors;
    }

    public void setAddedJurors(Integer[] addedJurors) {
        this.addedJurors = addedJurors;
    }
}
