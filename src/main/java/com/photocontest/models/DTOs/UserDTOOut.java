package com.photocontest.models.DTOs;

import com.photocontest.models.Image;
import com.photocontest.models.enums.Rank;

import java.util.Set;

public class UserDTOOut {

    private String firstName;
    private String lastName;
    private String username;
    private Rank rank;
    private Set<Image> images;

    public UserDTOOut() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public Set<Image> getImages() {
        return images;
    }

    public void setPhotos(Set<Image> images) {
        this.images = images;
    }
}
