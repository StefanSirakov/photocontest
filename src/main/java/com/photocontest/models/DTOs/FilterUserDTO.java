package com.photocontest.models.DTOs;

public class FilterUserDTO {
    private Integer minPoints;

    private Boolean role;

    private String search;

    public FilterUserDTO() {
    }

    public Integer getMinPoints() {
        return minPoints;
    }

    public void setMinPoints(Integer minPoints) {
        this.minPoints = minPoints;
    }

    public Boolean getRole() {
        return role;
    }

    public void setRole(Boolean role) {
        this.role = role;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
