package com.photocontest.models.DTOs;

import javax.validation.constraints.NotNull;

public class ContestInviteJunkiesDTO {

    @NotNull(message = "You should choose atleast 1 junkie.")
    private int[] invitedJunkies;

    public ContestInviteJunkiesDTO() {
    }

    public int[] getInvitedJunkies() {
        return invitedJunkies;
    }

    public void setInvitedJunkies(int[] invitedJunkies) {
        this.invitedJunkies = invitedJunkies;
    }
}
