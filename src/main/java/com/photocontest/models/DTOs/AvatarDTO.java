package com.photocontest.models.DTOs;

import org.springframework.web.multipart.MultipartFile;

public class AvatarDTO {

    private MultipartFile file;

    public AvatarDTO() {
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
