package com.photocontest.models.DTOs;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LoginDTO {

    @NotNull(message = "Username can't be empty")
    @Size(min = 3, max = 30, message = "Username should be between 3 and 30 symbols")
    private String username;

    @NotNull(message = "Password can't be empty")
    @Size(min = 6, max = 50, message = "Password should be between 6 and 50 symbols")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
