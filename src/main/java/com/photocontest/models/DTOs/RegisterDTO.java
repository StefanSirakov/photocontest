package com.photocontest.models.DTOs;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RegisterDTO extends LoginDTO {

    @NotNull(message = "Password can't be empty")
    @Size(min = 6, max = 50, message = "Password should be between 6 and 50 symbols")
    private String passwordConfirm;

    @NotNull(message = "First name can't be empty")
    @Size(min = 3, max = 30, message = "First name should be between 3 and 30 symbols")
    private String firstName;

    @NotNull(message = "Last name can't be empty")
    @Size(min = 3, max = 30, message = "Last name should be between 3 and 30 symbols")
    private String lastName;

    @NotNull(message = "Last name can't be empty")
    @Size(min = 6, max = 50, message = "Email should be between 6 and 50 symbols")
    private String email;

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}