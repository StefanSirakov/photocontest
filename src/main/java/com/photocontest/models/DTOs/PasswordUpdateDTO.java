package com.photocontest.models.DTOs;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PasswordUpdateDTO {

    @NotNull(message = "Old Password can't be empty")
    private String oldPassword;

    @NotNull(message = "Password can't be empty")
    @Size(min = 6, max = 30, message = "Password should be between 6 and 30 symbols")
    private String newPassword;

    @NotNull(message = "Password can't be empty")
    @Size(min = 6, max = 30, message = "Password should be between 6 and 30 symbols")
    private String newPasswordConfirm;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordConfirm() {
        return newPasswordConfirm;
    }

    public void setNewPasswordConfirm(String newPasswordConfirm) {
        this.newPasswordConfirm = newPasswordConfirm;
    }
}
