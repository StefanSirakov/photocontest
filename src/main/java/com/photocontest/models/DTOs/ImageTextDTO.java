package com.photocontest.models.DTOs;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ImageTextDTO {
    @NotNull(message = "Title should not be empty.")
    @Size(min = 5, max = 50, message = "Title should be between 5 and 50 symbols")
    private String title;

    @NotNull(message = "Story should not be empty.")
    @Size(min = 10, max = 1000, message = "Story should be between 10 and 10000 symbols")
    private String story;

    public ImageTextDTO() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }
}
