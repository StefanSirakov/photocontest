package com.photocontest.models.DTOs;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

public class ReviewDTO {

    private static final String PHOTO_REVIEW_SCORE_VALIDATION_MESSAGE = "Score must be in the interval 0-10";
    private static final String PHOTO_REVIEW_COMMENT_VALIDATION_MESSAGE = "Comment cannot be empty";
    private static final String PHOTO_REVIEW_COMMENT_LENGTH_VALIDATION_MESSAGE = "Comment must be 5 - 500 symbols long";

    @Range(min = 1, max = 10, message = PHOTO_REVIEW_SCORE_VALIDATION_MESSAGE)
    private int score;

    @NotNull(message = PHOTO_REVIEW_COMMENT_VALIDATION_MESSAGE)
    @Length(min = 5, max = 500, message = PHOTO_REVIEW_COMMENT_LENGTH_VALIDATION_MESSAGE)
    private String comment;

    private boolean isValid = true;

    public ReviewDTO() {
    }

    public ReviewDTO(int score, String comment) {
        this.score = score;
        this.comment = comment;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        this.isValid = valid;
    }
}
