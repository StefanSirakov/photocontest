package com.photocontest.models.DTOs;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PasswordDTO {
    @NotNull(message = "Password can't be empty")
    @Size(min = 3, max = 20, message = "Password should be between 3 and 20 symbols")
    private String oldPassword;

    private  String token;

    @NotNull(message = "Password can't be empty")
    @Size(min = 3, max = 20, message = "Password should be between 3 and 20 symbols")
    private String newPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
