package com.photocontest.models;

import javax.persistence.*;

@Entity
@Table(name = "contest_participants")
public class ContestParticipants {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "contest_id")
    private int contestId;

    @Column(name = "user_id")
    private int userId;

    public ContestParticipants() {
    }

    public ContestParticipants(int contestId, int userId) {
        this.contestId = contestId;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
