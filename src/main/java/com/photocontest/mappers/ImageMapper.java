package com.photocontest.mappers;

import com.photocontest.models.DTOs.ImageDTO;
import com.photocontest.models.Image;
import com.photocontest.models.User;
import com.photocontest.services.contracts.ImageService;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

@Component
public class ImageMapper {

    private final ImageService imageService;

    public ImageMapper(ImageService imageService) {
        this.imageService = imageService;
    }

    public Image fromDto(ImageDTO imageDTO, User user) {
        Image image = new Image();
        dtoToObject(imageDTO, image, user);
        return image;
    }

    public Image dtoToObject(ImageDTO imageDTO, Image image, User user){
        image.setTitle(imageDTO.getTitle());
        image.setStory(imageDTO.getStory());
        image.setUser(user);

        return image;
    }

    public Image fileToObject(int contestId, User user, Path path, String title, String story) {
        Image image = new Image();
        image.setContestId(contestId);
        image.setUser(user);
        image.setImageAbsolutePath(path.toString());
        image.setTitle(title);
        image.setStory(story);
        image.setTotalScore(0);
        return image;
    }
}
