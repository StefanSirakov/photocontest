package com.photocontest.mappers;

import com.photocontest.models.DTOs.ReviewDTO;
import com.photocontest.models.Review;
import com.photocontest.models.User;
import com.photocontest.services.contracts.ReviewService;
import org.springframework.stereotype.Component;

@Component
public class ReviewMapper {


    private final ReviewService reviewService;

    public ReviewMapper(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    public Review dtoToObject(ReviewDTO reviewDTO, User user) {
        Review review = new Review();
        review.setJuror(user);
        review.setScore(reviewDTO.getScore());
        review.setComment(reviewDTO.getComment());
        return review;
    }

}
