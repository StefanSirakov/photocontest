package com.photocontest.mappers;

import com.photocontest.models.Category;
import com.photocontest.models.Contest;
import com.photocontest.models.DTOs.ContestDTO;
import com.photocontest.models.User;
import com.photocontest.services.contracts.CategoryService;
import com.photocontest.services.contracts.ContestService;
import com.photocontest.services.contracts.UserService;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

@Component
public class ContestMapper {

    private final CategoryService categoryService;
    private final ContestService contestService;
    private final UserService userService;

    public ContestMapper(CategoryService categoryService, ContestService contestService, UserService userService) {
        this.categoryService = categoryService;
        this.contestService = contestService;
        this.userService = userService;
    }

    public Contest fromDto(ContestDTO contestDTO, int id) {
        Contest contest = contestService.getById(id);
        dtoToObject(contestDTO, contest);
        return contest;
    }

    public Contest fromDto(ContestDTO contestDTO , Path path) {
        Contest contest = new Contest();
        dtoToObject(contestDTO, contest);
        return contest;
    }
    public Contest dtoToObject(ContestDTO contestDTO, Contest contest, Path path) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        contest.setTitle(contestDTO.getTitle());
        Category category = categoryService.getByName(contestDTO.getCategory());
        contest.setCategory(category);
        contest.setIsOpen(contestDTO.getIsOpen());
        contest.setBackgroundImageAbsPath(path.toString());
        contest.setPhase(1);
        contest.setContest_starting_time(LocalDateTime.now());
        contest.setPhaseTwoStartingTime(LocalDateTime.parse(contestDTO.getPhaseTwoStartingTime(), formatter));
        contest.setContestEndingTime(LocalDateTime.parse(contestDTO.getContestEndingTime(), formatter));
        return contest;
    }

    public Contest dtoToObject(ContestDTO contestDTO, Contest contest) {
        contest.setTitle(contestDTO.getTitle());
        contest.setIsOpen(contestDTO.getIsOpen());
        contest.setPhase(1);
        contest.setContest_starting_time(LocalDateTime.now());
        contest.setPhaseTwoStartingTime(LocalDateTime.parse(contestDTO.getPhaseTwoStartingTime()));
        contest.setContestEndingTime(LocalDateTime.parse(contestDTO.getContestEndingTime()));

        Set<User> jurors = new HashSet<>(userService.getAllOrganizers());


        if (contestDTO.getAddedJurors() != null) {
            for (int addedJurorId : contestDTO.getAddedJurors()) {
                jurors.add(userService.getById(addedJurorId));
            }
        }

        contest.setJurry(jurors);
        return contest;
    }

    public Contest dtoToObject(ContestDTO contestDTO) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:MM");
        Contest contest = new Contest();
        contest.setTitle(contestDTO.getTitle());

        contest.setIsOpen(contestDTO.getIsOpen());
        contest.setPhase(1);
        contest.setContest_starting_time(LocalDateTime.now());
        contest.setPhaseTwoStartingTime(LocalDateTime.parse(contestDTO.getPhaseTwoStartingTime(), formatter));
        contest.setContestEndingTime(LocalDateTime.parse(contestDTO.getContestEndingTime(), formatter));
        return contest;
    }
}
