package com.photocontest.mappers;

import com.photocontest.models.DTOs.RegisterDTO;
import com.photocontest.models.DTOs.UserProfileUpdateDTO;
import com.photocontest.models.User;
import com.photocontest.services.contracts.UserRankService;
import com.photocontest.services.contracts.UserService;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public static final String DEFAULT_AVATAR_PATH = "C:\\photo-contest\\src\\main\\resources\\static\\assets\\images\\default_avatar\\default_avatar.jpg";
    private final UserService userService;
    private final UserRankService userRankService;

    public UserMapper(UserService userService, UserRankService userRankService) {
        this.userService = userService;
        this.userRankService = userRankService;
    }

    public User dtoToObject(RegisterDTO registerDTO) {
        User user = new User();
        user.setFirstName(registerDTO.getFirstName());
        user.setLastName(registerDTO.getLastName());
        user.setPassword(registerDTO.getPassword());
        user.setUsername(registerDTO.getUsername());
        user.setEmail(registerDTO.getEmail());
        user.setOrganiser(false);
        user.setDeleted(false);
        user.setUserRank(userRankService.getById(1));
        user.setPoints(0);
        user.setAvatarAbsPath(DEFAULT_AVATAR_PATH);
        return user;
    }

    public User dtoToObjectForUpdate(UserProfileUpdateDTO userProfileUpdateDTO, int userId) {
        User user = userService.getById(userId);
        user.setFirstName(userProfileUpdateDTO.getFirstName());
        user.setLastName(userProfileUpdateDTO.getLastName());
        user.setEmail(userProfileUpdateDTO.getEmail());
        return user;
    }

    public UserProfileUpdateDTO objectToDTO(User user){
        UserProfileUpdateDTO dto = new UserProfileUpdateDTO();
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setUsername(user.getUsername());
        dto.setEmail(user.getEmail());
        dto.setAvatar(user.getAvatarRelPath());
        return dto;
    }
}

