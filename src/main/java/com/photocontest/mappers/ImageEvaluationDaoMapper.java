package com.photocontest.mappers;

import com.photocontest.models.Image;
import com.photocontest.models.Review;
import com.photocontest.models.daos.ImageEvaluationDao;
import org.springframework.stereotype.Component;

import java.util.Set;
@Component
public class ImageEvaluationDaoMapper {

    public ImageEvaluationDaoMapper() {
    }

    public ImageEvaluationDao compileImageEvaluation(Image image, Set<Review> reviews, int score,
                                                     int rankingPlace) {
        return new ImageEvaluationDao(image, reviews, score, rankingPlace);
    }
}
