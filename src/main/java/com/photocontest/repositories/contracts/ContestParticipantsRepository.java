package com.photocontest.repositories.contracts;

import com.photocontest.models.ContestParticipants;

import java.util.List;

public interface ContestParticipantsRepository extends BaseCRUDRepository<ContestParticipants>{
    List<ContestParticipants> getByUserEntry(int userId);
}
