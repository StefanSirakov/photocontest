package com.photocontest.repositories.contracts;

import com.photocontest.models.ContestWinner;

import java.util.List;

public interface ContestWinnerRepostitory extends BaseCRUDRepository<ContestWinner> {
    List<ContestWinner> getTopPhotos();
}
