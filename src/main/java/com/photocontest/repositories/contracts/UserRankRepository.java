package com.photocontest.repositories.contracts;

import com.photocontest.models.UserRank;

public interface UserRankRepository extends BaseReadRepository<UserRank>{
}
