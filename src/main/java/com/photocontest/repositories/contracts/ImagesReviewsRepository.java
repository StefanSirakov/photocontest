package com.photocontest.repositories.contracts;

import com.photocontest.models.ImagesReviews;

public interface ImagesReviewsRepository extends BaseCRUDRepository<ImagesReviews> {
}
