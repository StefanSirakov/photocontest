package com.photocontest.repositories.contracts;

import com.photocontest.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseCRUDRepository<User> {

    List<User> search(Optional<String> search);

    List<User> getAllOrganizers();

    List<User> getAllEligibleForJuror(int master, int wbpd);

    List<User> getAllJunkies();

    List<User> getAllOrderedByPoints();

    List<User> filter(Optional<String> search, Optional<Integer> minPoints, Optional<Boolean> role);
}
