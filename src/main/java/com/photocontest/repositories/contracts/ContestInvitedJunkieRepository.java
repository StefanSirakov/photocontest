package com.photocontest.repositories.contracts;

import com.photocontest.models.ContestInvitedJunkie;

public interface ContestInvitedJunkieRepository extends BaseCRUDRepository<ContestInvitedJunkie>{
}
