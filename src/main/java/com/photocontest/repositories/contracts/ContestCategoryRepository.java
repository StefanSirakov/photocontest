package com.photocontest.repositories.contracts;

import com.photocontest.models.ContestCategory;

public interface ContestCategoryRepository extends BaseCRUDRepository<ContestCategory> {
}
