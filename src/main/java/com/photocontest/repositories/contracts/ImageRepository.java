package com.photocontest.repositories.contracts;


import com.photocontest.models.Image;

import java.util.List;
import java.util.Optional;

public interface ImageRepository extends BaseCRUDRepository<Image> {
    List<Image> search(Optional<String> search, int contestId);

    List<Image> getByUserId(int id);
}
