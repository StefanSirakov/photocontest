package com.photocontest.repositories.contracts;

import com.photocontest.models.ContestsJurors;

import java.util.List;

public interface ContestsJurorsRepository extends BaseCRUDRepository<ContestsJurors>  {

    List<ContestsJurors> getByUserId(int userId);
}
