package com.photocontest.repositories.contracts;

import com.photocontest.models.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends BaseCRUDRepository<Category> {

    List<Category> search(Optional<String> search);
}
