package com.photocontest.repositories.contracts;

import com.photocontest.models.Contest;

import java.util.List;
import java.util.Optional;

public interface ContestRepository extends BaseCRUDRepository<Contest> {

    List<Contest> search(Optional<String> search);

    List<Contest> filter(Optional<String> title, Optional<Integer> category, Optional<Integer> phase);

//    void submitPhoto(Photo photo);
//
//    void ratePhote();




}
