package com.photocontest.repositories;

import com.photocontest.models.Contest;
import com.photocontest.repositories.contracts.CategoryRepository;
import com.photocontest.repositories.contracts.ContestRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class ContestRepositoryImpl extends AbstractCRUDRepositoryImpl<Contest> implements ContestRepository {

    private final SessionFactory sessionFactory;
    private final CategoryRepository categoryRepository;

    @Autowired
    public ContestRepositoryImpl(SessionFactory sessionFactory, CategoryRepository categoryRepository) {
        super(Contest.class, sessionFactory);
        this.sessionFactory = sessionFactory;
        this.categoryRepository = categoryRepository;
    }


    @Override
    public List<Contest> search(Optional<String> search) {
        if (search.isEmpty()) {
            return getAll();
        }
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where title like :title or " +
                    "creator.username like :username or " +
                    "category.name like :category");
            query.setParameter("title", "%" + search.get() + "%");
            query.setParameter("username", "%" + search.get() + "%");
            query.setParameter("category", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<Contest> filter(Optional<String> title, Optional<Integer> category, Optional<Integer> phase) {
        try (Session session = sessionFactory.openSession()) {
            var baseQueryString = new StringBuilder(" from Contest ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();
//            final String[] name = {""};
//            final Integer[] phaseNum = new Integer[1];

            title.ifPresent(value -> {
                filter.add(" title like :title");
                queryParams.put("title", "%" + value + "%");
            });

            category.ifPresent(value -> {
                filter.add(" category.id = :categoryId");
                queryParams.put("categoryId", value);
            });

            phase.ifPresent(value -> {
                filter.add(" phase = :phaseId");
                queryParams.put("phaseId", value);
            });

            if (!filter.isEmpty()) {
                baseQueryString.append(" where ").append(String.join(" and ", filter));
            }

//            sortOptions.ifPresent(value -> baseQueryString.append(value.getQuery()));

            return session.createQuery(baseQueryString.toString(), Contest.class)
                    .setProperties(queryParams)
                    .list();
        }
    }


}
