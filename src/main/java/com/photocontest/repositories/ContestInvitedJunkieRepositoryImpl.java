package com.photocontest.repositories;

import com.photocontest.models.ContestInvitedJunkie;
import com.photocontest.repositories.contracts.ContestInvitedJunkieRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ContestInvitedJunkieRepositoryImpl extends AbstractCRUDRepositoryImpl<ContestInvitedJunkie> implements ContestInvitedJunkieRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public ContestInvitedJunkieRepositoryImpl(SessionFactory sessionFactory) {
        super(ContestInvitedJunkie.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }


}
