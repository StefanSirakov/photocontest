package com.photocontest.repositories;

import com.photocontest.models.ImagesReviews;
import com.photocontest.repositories.contracts.ImagesReviewsRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ImagesReviewsRepositoryImpl extends AbstractCRUDRepositoryImpl<ImagesReviews> implements ImagesReviewsRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ImagesReviewsRepositoryImpl(SessionFactory sessionFactory) {
        super(ImagesReviews.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
