package com.photocontest.repositories;

import com.photocontest.models.Review;
import com.photocontest.repositories.contracts.ReviewRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ReviewRepositoryImpl extends AbstractCRUDRepositoryImpl<Review> implements ReviewRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ReviewRepositoryImpl(SessionFactory sessionFactory) {
        super(Review.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
