package com.photocontest.repositories;

import com.photocontest.models.ContestCategory;
import com.photocontest.repositories.contracts.ContestCategoryRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ContestCategoryRepositoryImpl extends AbstractCRUDRepositoryImpl<ContestCategory> implements ContestCategoryRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public ContestCategoryRepositoryImpl(SessionFactory sessionFactory) {
        super(ContestCategory.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
