package com.photocontest.repositories;

import com.photocontest.models.ContestsJurors;
import com.photocontest.repositories.contracts.ContestsJurorsRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ContestsJurorsRepositoryImpl extends AbstractCRUDRepositoryImpl<ContestsJurors> implements ContestsJurorsRepository {

    private SessionFactory sessionFactory;

    public ContestsJurorsRepositoryImpl(SessionFactory sessionFactory) {
        super(ContestsJurors.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ContestsJurors> getByUserId(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestsJurors> query = session.createQuery("from ContestsJurors where juror.id = :jurorId");
            query.setParameter("jurorId", userId);
            return query.list();
        }
    }
}
