package com.photocontest.repositories;

import com.photocontest.models.Image;
import com.photocontest.repositories.contracts.ImageRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ImageRepositoryImpl extends AbstractCRUDRepositoryImpl<Image> implements ImageRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ImageRepositoryImpl(SessionFactory sessionFactory) {
        super(Image.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Image> search(Optional<String> search, int contestId) {
        if (search.isEmpty()) {
            try (Session session = sessionFactory.openSession()) {
                Query<Image> query = session.createQuery("from Image where contestId = :contestId");
                query.setParameter("contestId", contestId);
                return query.list();
            }
        } else {
            try (Session session = sessionFactory.openSession()) {
                Query<Image> query = session.createQuery(
                        "from Image where contestId = :contestId and title like :title");
                query.setParameter("contestId", contestId);
                query.setParameter("title", "%" + search.get() + "%");
                return query.list();
            }
        }
    }

    @Override
    public List<Image> getByUserId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Image> query = session.createQuery("from Image where user.id = :userId");
            query.setParameter("userId", id);
            return query.list();
        }
    }
}
