package com.photocontest.repositories;

import com.photocontest.models.Category;
import com.photocontest.repositories.contracts.CategoryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CategoryRepositoryImpl extends AbstractCRUDRepositoryImpl<Category> implements CategoryRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        super(Category.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Category> search(Optional<String> search) {
        if (search.isEmpty()) {
            return getAll();
        }
        try (Session session = sessionFactory.openSession()) {
            Query<Category> query = session.createQuery("from Category where name like :name or ");
            query.setParameter("name", "%" + search.get() + "%");
            return query.list();
        }
    }
}
