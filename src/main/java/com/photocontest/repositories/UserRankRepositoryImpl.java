package com.photocontest.repositories;

import com.photocontest.models.UserRank;
import com.photocontest.repositories.contracts.UserRankRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRankRepositoryImpl extends AbstractReadRepositoryImpl<UserRank> implements UserRankRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public UserRankRepositoryImpl(SessionFactory sessionFactory) {
        super(UserRank.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
