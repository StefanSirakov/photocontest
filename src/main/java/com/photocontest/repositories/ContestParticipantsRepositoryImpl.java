package com.photocontest.repositories;

import com.photocontest.models.ContestParticipants;
import com.photocontest.repositories.contracts.ContestParticipantsRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ContestParticipantsRepositoryImpl extends AbstractCRUDRepositoryImpl<ContestParticipants> implements ContestParticipantsRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public ContestParticipantsRepositoryImpl(SessionFactory sessionFactory ) {
        super(ContestParticipants.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ContestParticipants> getByUserEntry(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestParticipants> query = session.createQuery("from ContestParticipants where userId = :userId");
            query.setParameter("userId", userId);
            return query.list();
        }
    }

}
