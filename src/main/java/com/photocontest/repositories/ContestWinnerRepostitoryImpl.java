package com.photocontest.repositories;

import com.photocontest.models.ContestWinner;
import com.photocontest.repositories.contracts.ContestWinnerRepostitory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ContestWinnerRepostitoryImpl extends AbstractCRUDRepositoryImpl<ContestWinner> implements ContestWinnerRepostitory {

    private SessionFactory sessionFactory;

    public ContestWinnerRepostitoryImpl(SessionFactory sessionFactory) {
        super(ContestWinner.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ContestWinner> getTopPhotos() {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestWinner> query = session.createQuery("from ContestWinner where place = :place");
            query.setParameter("place", 1);

            return query.getResultList();
        }
    }
}
