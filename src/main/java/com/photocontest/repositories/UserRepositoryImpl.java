package com.photocontest.repositories;

import com.photocontest.models.User;
import com.photocontest.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends AbstractCRUDRepositoryImpl<User> implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> search(Optional<String> search) {
        if (search.isEmpty()) {
            return getAllOrderedByPoints();
        }
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username like :username or " +
                    "firstName like :firstName or " +
                    "lastName like :lastName");
            query.setParameter("username", "%" + search.get() + "%");
            query.setParameter("lastName", "%" + search.get() + "%");
            query.setParameter("firstName", "%" + search.get() + "%");
            return query.list();
        }
    }

    @Override
    public List<User> getAllOrganizers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where isOrganiser = :true");
            query.setParameter("true", true);
            return query.list();
        }
    }

    @Override
    public List<User> getAllEligibleForJuror(int master, int wbpd) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where rank.id = :master or rank.id = :wbpd");
            query.setParameter("master", master);
            query.setParameter("wbpd", wbpd);
            return query.list();
        }
    }

    @Override
    public List<User> getAllJunkies() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where isOrganiser = :false");
            query.setParameter("false", false);
            return query.list();
        }
    }

    @Override
    public List<User> getAllOrderedByPoints() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User order by points desc");
            return query.list();
        }
    }

    @Override
    public List<User> filter(Optional<String> search, Optional<Integer> minPoints, Optional<Boolean> role) {
        try (Session session = sessionFactory.openSession()) {
            var baseQueryString = new StringBuilder(" from User ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();
//            final String[] name = {""};
//            final Integer[] phaseNum = new Integer[1];

            search.ifPresent(value -> {
                filter.add(" firstName like :firstName or lastName like :lastName or username like :username");
                queryParams.put("firstName", "%" + value + "%");
                queryParams.put("lastName", "%" + value + "%");
                queryParams.put("username", "%" + value + "%");
            });

            minPoints.ifPresent(value -> {
                filter.add(" points >= :points");
                queryParams.put("points", value);
            });

            role.ifPresent(value -> {
                filter.add(" isOrganiser = :isOrganiser");
                queryParams.put("isOrganiser", value);
            });

            if (!filter.isEmpty()) {
                baseQueryString.append(" where ").append(String.join(" and ", filter));
            }

//            sortOptions.ifPresent(value -> baseQueryString.append(value.getQuery()));

            return session.createQuery(baseQueryString.toString(), User.class)
                    .setProperties(queryParams)
                    .list();
        }
    }
}
