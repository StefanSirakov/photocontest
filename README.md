## **Best Shot**
### `by Stefan Sirakov & Iskren Danchev`
 Best Shot is an application that allows users to enroll and participate in online contests with other photo junkies by uploading images in various cateogries. The organizational side ot the application allows creation, management and evaluation of those photo contests in question. Users accumulate points and gain ranks. High-ranked photo junkies can be elected as contest jurors.

---

### [**Swagger Doc**]( http://localhost:8080/swagger-ui/#/ )

---
- ## Project initialization
    - You can either download the project files in a format of your preference and then unpack the files in a new folder or you can clone the repository in a folder of your choosing. Please see below for more details:
    - ### How to download project files: 
  ![img.png](READMEimages/img.png) 
    - Go to a new folder and unpack the files if you have downloaded the project
    - ### To clone the repository, please copy the HTTPS link:
  ![img_1.png](READMEimages/img_1.png)
    - Clone the repository via the git clone command:
  ![img_2.png](READMEimages/img_2.png) 
  and paste the HTTPS link.

- ## How to create and fill the database with data:
    - On the right of your screen you should see the database tab. Click on it and select the + sign.
      From there select Data Source and from the drop-down list select MariaDB.
  
  ![img_8.png](READMEimages/img_8.png)
    - Login with your credentials and start the database.
    - Open the project tab and find the db directory.
      Open create.sql. This script will create the scheme in the database and all its tables and relations.![img_4.png](READMEimages/img_4.png)

    - You can execute the script, select all lines, right click and Execute via the Console.
    - Now you should be able to see the created scheme and tables in the database:
  
  ![img_5.png](READMEimages/img_5.png)
  - After this is done you must select the script - insert-data.sql, to fill in the database tables with information. You may execute it in the same way as the last script.
  - Go to /photo-contest/src/main/resources/application.properties and update the database.username and database.password to match yours.
  - You must also update the Strings below based on where you have saved the project. They are in ContestMVCController and UserMVCController
  ![img_6.png](READMEimages/img_6.png)
- The application has Rest controllers which allow for the the main features to be used with developer tools such as Postman.

![img_7.png](READMEimages/img_7.png)
  - The contest controller allows for organizers to create contests and review images. Allows all users to upload images and see all contests in all phases.
  - The category controller allows users to see all categories.
  - The user controller allows new users to register, see all users and update/delete profile.
  
  - ### Front-end usage. 
    - To use the application features more easily you must start the server and make sure that everything loads correctly.
    - Open a browser and got to http://localhost:8080/
    - The home page will load and you will be able to see the latest contest winning images and options to register or to log in.
    ![img.png](READMEimages/img_10.png)
    - Once you login you will be able to see all contests and images, to participate in contests and manage your profile, if you are a jurry you will be able to review images. If you are an organizer you will be able to create contests, promote users to organizer and review images.

  - ### Database Relations
![img_1.png](READMEimages/img_9.png)

